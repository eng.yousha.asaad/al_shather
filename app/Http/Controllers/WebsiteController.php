<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Brand;
use App\Models\Client;
use App\Models\Project;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session as FacadesSession;

class WebsiteController extends Controller
{
    public function index(){
        dd('vds');
        return view('pages.website.home')
        ->withBrands(Brand::getAll())
        ->withProjects(Project::getAll())
        ->withActivities(Activity::getAll())
        ->withClients(Client::getAll());

    }


    public function showServices(){
        App::setLocale(FacadesSession::get('lang'));
        return view('pages.website.services');
    }
}
