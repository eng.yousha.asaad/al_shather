<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        App::setLocale(Session::get('lang'));
       return view('pages.brand.index')->withBrands(Brand::getAll());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all() , [
            'brand_name' => 'required|max:40',
            'description' => 'required',
            'images' => 'required'
        ]);

        if($validator->fails())
        {
           Session::flash('fail' , "Try Again");
           return redirect()->back();
        }else
        {
           DB::beginTransaction();

           try{
               $new_brand = Brand::create([
                'brand_name' => $request->brand_name,
                'description' => $request->description,
               ]);

               if($new_brand != null)
               {
                   //add images for this brand
                   foreach($request->file('images') as $image)
                   {
                       $random_image_name = uniqid().'.'.$image->clientExtension();
                       $image->move(public_path('assets/images/brand'), $random_image_name);

                       //add image for Images table
                       $image_data = new Image();
                       $image_data->path = env('APP_URL').'/assets/images/brand/'.$random_image_name;

                       $new_brand->images()->save($image_data);
                   }

                   //add brand data to translation file
                   storeSetneceInLanguageFile([$request->brand_name , $request->description]);
                   DB::commit();
                   Session::flash('success' , 'Done Successfully');
               }
           }catch(\Exception $ex)
           {
               DB::rollBack();
               Log::info('Error in add brand');
               Log::info($ex->getMessage());
           }

           return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        $validator = Validator::make($request->all() , [
            'brand_name' => 'required|max:40',
            'description' => 'required',
        ]);

        if($validator->fails())
        {
           Session::flash('fail' , "Try Again");
           return redirect()->back();
        }else
        {
           DB::beginTransaction();

           try{

               //chnage sentecnes in language file
               replaceSetneceInLanguageFile([
                   $brand->brand_name => $request->brand_name,
                   $brand->description => $request->description,
               ]);

               //changeable
               $updated_brand = Brand::update_data($brand->id ,[
                'brand_name' => $request->brand_name,
                'description' => $request->description,
               ]);
                   //add images for this brand
                   //here user might not add any extra image
                   if($request->file('images') != null )
                   {

                        foreach($request->file('images') as $image)
                        {
                            $random_image_name = uniqid().'.'.$image->clientExtension();
                            $image->move(public_path('assets/images/brand'), $random_image_name);

                            //add image for Images table
                            $image_data = new Image();
                            $image_data->path = env('APP_URL').'/assets/images/brand/'.$random_image_name;

                            $brand->images()->save($image_data);
                        }
                   }


               DB::commit();
               Session::flash('success' , 'Done Successfully');
           }catch(\Exception $ex)
           {
               dd($ex->getMessage());
               DB::rollBack();
               Log::info('Error in update brand');
               Log::info($ex->getMessage());
           }

           return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        DB::beginTransaction();
        try{
            $data = [$brand->brand_name , $brand->description];

           //here we will delete brand with its images

           //delete images
           foreach($brand->images as $image)
           {
               //get image name with extension then delete it from public folder
               $image_name = substr($image->path , strrpos($image->path , '/') +1);
               File::delete(public_path('assets/images/brand/'.$image_name));
               $image->delete();
           }

           $brand->delete();
           deleteSetneceInLanguageFile($data);
           DB::commit();
           Session::flash('success' , 'Done Successfully');


        }catch(\Exception $ex)
        {
            Session::flash('fail' , "Try Again");
            DB::rollBack();
            Log::info($ex->getMessage());
        }

        return redirect()->back();
    }

    public function deleteGroup(Request $request)
    {
        DB::beginTransaction();
        try{
            unset($request['_token']);
            $deleted_brands_ids = array_values($request->all());
            foreach($deleted_brands_ids as $id)
            {
                //here we will delete brand with its images
                $brand = Brand::getById($id);


                //delete images
                foreach($brand->images as $image)
                {
                    //get image name with extension then delete it from public folder
                    $image_name = substr($image->path , strrpos($image->path , '/') +1);
                    File::delete(public_path('assets/images/brand/'.$image_name));
                    $image->delete();
                }

                $brand->delete();

            }

            DB::commit();
            Session::flash('success' , 'Done Successfully');


        }catch(\Exception $ex)
        {
            Session::flash('fail' , "Try Again");
            DB::rollBack();
            Log::info($ex->getMessage());
        }

        return redirect()->back();
    }

    public function showCategories($brand_id)
    {
        App::setlocale(Session::get('lang'));
        $categories = Category::getCategoriesForBrand($brand_id);
        $brand_name = Brand::getById($brand_id)->brand_name;

        return view('pages.brand_categories')->with(compact('categories' , 'brand_name'));
    }
}
