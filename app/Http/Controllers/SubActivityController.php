<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Image;
use App\Models\SubActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class SubActivityController extends Controller
{

    public static $images_path =  '/assets/images/sub_activity/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        App::setLocale(Session::get('lang'));
        return view('pages.sub_activity.index')->withSubactivities(SubActivity::getAll())->withactivities(Activity::getAll());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all() , [
            'sub_activity_name' => 'required|max:40',
            'description' => 'required',
            'activity_id' => 'required',
            'images' => 'required'
        ]);

        if($validator->fails())
        {
           Session::flash('fail' , "Try Again");
           return redirect()->back();
        }else
        {
           DB::beginTransaction();

           try{
               $new_sub_activity = SubActivity::create([
                'sub_activity_name' => $request->sub_activity_name,
                'description' => $request->description,
                'activity_id' => $request->activity_id
               ]);

               if($new_sub_activity != null)
               {
                   //add images for this activity
                   foreach($request->file('images') as $image)
                   {
                       $random_image_name = uniqid().'.'.$image->clientExtension();
                       $image->move(public_path(self::$images_path), $random_image_name);

                       //add image for Images table
                       $image_data = new Image();
                       $image_data->path = env('APP_URL').self::$images_path.$random_image_name;

                       $new_sub_activity->images()->save($image_data);
                   }

                   //add brand data to translation file
                  storeSetneceInLanguageFile([$request->sub_activity_name , $request->description]);

                   DB::commit();
                   Session::flash('success' , 'Done Successfully');
               }
           }catch(\Exception $ex)
           {
               DB::rollBack();
               Log::info('Error in add Sub Sub ACtivity');
               Log::info($ex->getMessage());
           }

           return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SubActivity  $sub_acticity
     * @return \Illuminate\Http\Response
     */
    public function show(SubActivity $sub_acticity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SubActivity  $sub_acticity
     * @return \Illuminate\Http\Response
     */
    public function edit(SubActivity $sub_acticity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SubActivity  $subActivity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubActivity $subActivity)
    {

        $validator = Validator::make($request->all() , [
            'sub_activity_name' => 'required|max:40',
            'description' => 'required',
            'activity_id' => 'required',
        ]);

        if($validator->fails())
        {
           Session::flash('fail' , "Try Again");
           return redirect()->back();
        }else
        {
           DB::beginTransaction();

           try{
                //chnage sentecnes in language file
              replaceSetneceInLanguageFile([
                $subActivity->sub_activity_name => $request->sub_activity_name,
                $subActivity->description => $request->description,
               ]);

               //changeable
               $updated_sub_activity = SubActivity::update_data($subActivity->id ,[
                'sub_activity_name' => $request->sub_activity_name,
                'description' => $request->description,
                'activity_id' => $request->activity_id,
               ]);
                   //add images for this sub activity
                   //here user might not add any extra image
                   if($request->file('images') != null )
                   {

                        foreach($request->file('images') as $image)
                        {
                            $random_image_name = uniqid().'.'.$image->clientExtension();
                            $image->move(public_path(self::$images_path), $random_image_name);

                            //add image for Images table
                            $image_data = new Image();
                            $image_data->path = env('APP_URL').self::$images_path.$random_image_name;

                            $subActivity->images()->save($image_data);
                        }
                   }


               DB::commit();
               Session::flash('success' , 'Done Successfully');
           }catch(\Exception $ex)
           {
               dd($ex->getMessage());
               DB::rollBack();
               Log::info('Error in update sub category');
               Log::info($ex->getMessage());
           }

           return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SubActivity  $sub_acticity
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubActivity $subActivity)
    {

        DB::beginTransaction();
        try{
            $data = [$subActivity->sub_activity_name , $subActivity->description];
           //here we will delete sub activity with its images

           //delete images
           foreach($subActivity->images as $image)
           {
               //get image name with extension then delete it from public folder
               $image_name = substr($image->path , strrpos($image->path , '/') +1);
               File::delete(public_path(self::$images_path.$image_name));
               $image->delete();
           }

           $subActivity->delete();
           deleteSetneceInLanguageFile($data);
           DB::commit();
           Session::flash('success' , 'Done Successfully');


        }catch(\Exception $ex)
        {
            Session::flash('fail' , "Try Again");
            DB::rollBack();
            Log::info($ex->getMessage());
        }
        return redirect()->back();
    }

    public function deleteGroup(Request $request)
    {
        DB::beginTransaction();

        try{

            unset($request['_token']);
            $deleted_categories_ids = array_values($request->all());

            foreach($deleted_categories_ids as $id)
            {
                //here we will delete sub activity with its images
                $sub_acticity = SubActivity::getById($id);

                //delete images
                foreach($sub_acticity->images as $image)
                {
                    //get image name with extension then delete it from public folder
                    $image_name = substr($image->path , strrpos($image->path , '/') +1);
                    File::delete(public_path(self::$images_path.$image_name));
                    $image->delete();
                }

                $sub_acticity->delete();
            }

            DB::commit();
            Session::flash('success' , 'Done Successfully');
        }catch(\Exception $ex)
        {
            Session::flash('fail' , "Try Again");
            DB::rollBack();
            Log::info($ex->getMessage());
        }

        return redirect()->back();
    }
}
