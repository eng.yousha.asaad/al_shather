<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Image;
use App\Models\SubActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ActivityController extends Controller
{ /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index()
   {
      App::setLocale(Session::get('lang'));
      return view('pages.activity.index')->withActivities(Activity::getAll());
   }

   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
       //
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
       $validator = Validator::make($request->all() , [
           'activity_name' => 'required|max:40',
           'description' => 'required',
           'images' => 'required'
       ]);

       if($validator->fails())
       {
          Session::flash('fail' , "Try Again");
          return redirect()->back();
       }else
       {
          DB::beginTransaction();

          try{
              $new_activity = Activity::create([
               'activity_name' => $request->activity_name,
               'description' => $request->description,
              ]);


              if($new_activity != null)
              {
                  //add images for this activity
                  foreach($request->file('images') as $image)
                  {
                      $random_image_name = uniqid().'.'.$image->clientExtension();
                      $image->move(public_path('assets/images/activity'), $random_image_name);

                      //add image for Images table
                      $image_data = new Image();
                      $image_data->path = env('APP_URL').'/assets/images/activity/'.$random_image_name;

                      $new_activity->images()->save($image_data);
                  }

                  //add brand data to translation file
                  storeSetneceInLanguageFile([$request->activity_name , $request->description]);

                  DB::commit();
                  Session::flash('success' , 'Done Successfully');
              }
          }catch(\Exception $ex)
          {
              DB::rollBack();
              Log::info('Error in add activity');
              Log::info($ex->getMessage());
          }

          return redirect()->back();
       }
   }

   /**
    * Display the specified resource.
    *
    * @param  \App\Models\Activity  $activity
    * @return \Illuminate\Http\Response
    */
   public function show(Activity $activity)
   {
       App::setLocale(Session::get('lang'));
       return view('pages.website.sub_activities')
       ->withActivityname($activity->activity_name)
       ->withActivities(SubActivity::getSubActivitiesForActivity($activity->id));
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Models\Activity  $activity
    * @return \Illuminate\Http\Response
    */
   public function edit(Activity $activity)
   {
       //
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Models\Activity  $activity
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, Activity $activity)
   {
       $validator = Validator::make($request->all() , [
           'activity_name' => 'required|max:40',
           'description' => 'required',
       ]);

       if($validator->fails())
       {
          Session::flash('fail' , "Try Again");
          return redirect()->back();
       }else
       {
          DB::beginTransaction();

          try{
               //chnage sentecnes in language file
               replaceSetneceInLanguageFile([
                $activity->activity_name => $request->activity_name,
                $activity->description => $request->description,
               ]);

              //changeable
              $updated_activity = Activity::update_data($activity->id ,[
               'activity_name' => $request->activity_name,
               'description' => $request->description,
              ]);
                  //add images for this activity
                  //here user might not add any extra image
                  if($request->file('images') != null )
                  {

                       foreach($request->file('images') as $image)
                       {
                           $random_image_name = uniqid().'.'.$image->clientExtension();
                           $image->move(public_path('assets/images/activity'), $random_image_name);

                           //add image for Images table
                           $image_data = new Image();
                           $image_data->path = env('APP_URL').'/assets/images/activity/'.$random_image_name;

                           $activity->images()->save($image_data);
                       }
                  }


              DB::commit();
              Session::flash('success' , 'Done Successfully');
          }catch(\Exception $ex)
          {
              dd($ex->getMessage());
              DB::rollBack();
              Log::info('Error in update activity');
              Log::info($ex->getMessage());
          }

          return redirect()->back();
       }
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Models\Activity  $activity
    * @return \Illuminate\Http\Response
    */
   public function destroy(Activity $activity)
   {
       DB::beginTransaction();

       try{
           $data =[$activity->activity_name , $activity->description];
          //here we will delete activity with its images

          //delete images
          foreach($activity->images as $image)
          {
              //get image name with extension then delete it from public folder
              $image_name = substr($image->path , strrpos($image->path , '/') +1);
              File::delete(public_path('assets/images/activity/'.$image_name));
              $image->delete();
          }

          $activity->delete();
          deleteSetneceInLanguageFile($data);
          DB::commit();
          Session::flash('success' , 'Done Successfully');


       }catch(\Exception $ex)
       {
           Session::flash('fail' , "Try Again");
           DB::rollBack();
           Log::info($ex->getMessage());
       }

       return redirect()->back();
   }

   public function deleteGroup(Request $request)
   {
       DB::beginTransaction();
       try{
           unset($request['_token']);
           $deleted_activities_ids = array_values($request->all());
           foreach($deleted_activities_ids as $id)
           {
               //here we will delete activity with its images
               $activity = Activity::getById($id);


               //delete images
               foreach($activity->images as $image)
               {
                   //get image name with extension then delete it from public folder
                   $image_name = substr($image->path , strrpos($image->path , '/') +1);
                   File::delete(public_path('assets/images/activity/'.$image_name));
                   $image->delete();
               }

               $activity->delete();

           }

           DB::commit();
           Session::flash('success' , 'Done Successfully');


       }catch(\Exception $ex)
       {
           Session::flash('fail' , "Try Again");
           DB::rollBack();
           Log::info($ex->getMessage());
       }

       return redirect()->back();
   }


}
