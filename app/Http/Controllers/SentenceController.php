<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class SentenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        App::setLocale(Session::get('lang'));
        $sentences = require resource_path('lang/ar/sentence.php');
        return view('pages.sentence.index')->with(compact('sentences'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try{
            unset($request['_token']);
            unset($request['_method']);


            $sentences = require resource_path('lang/ar/sentence.php');

            foreach($request->all() as $english => $arabic)
            {
                $sentences[str_replace('_' , ' ' , $english)] = $arabic;
            }

            //then we store array in sentence language file
            file_put_contents(resource_path('lang/ar/sentence.php'), "<?php \n return " . var_export($sentences, true) . ";");

            Session::flash('success' , 'Done Successfully');
        }catch(\Exception $ex)
        {

            Log::info('ERROR in TRANSLATE SENTENCES');
            Log::info($ex->getMessage());
        }
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
