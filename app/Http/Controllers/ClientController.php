<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class ClientController extends Controller
{
    public static $images_path =  '/assets/images/client/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        App::setLocale(Session::get('lang'));
        return view('pages.client.index')->withClients(Client::getAll());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all() , [
            'client_name' => 'required',
            'logo' => 'required'
        ]);

        if($validator->fails())
        {
           Session::flash('fail' , "Try Again");
           return redirect()->back();
        }else
        {
           DB::beginTransaction();

           try{
               $new_client = Client::create([
                'client_name' => $request->client_name,
               ]);


               if($new_client != null)
               {
                   //add images for this client
                   $image = $request->file('logo');

                    $random_image_name = uniqid().'.'.$image->clientExtension();
                    $image->move(public_path(self::$images_path), $random_image_name);

                    //add image for Images table
                    $image_data = new Image();
                    $image_data->path = env('APP_URL').self::$images_path.$random_image_name;

                    $new_client->images()->save($image_data);


                   DB::commit();
                   Session::flash('success' , 'Done Successfully');
               }
           }catch(\Exception $ex)
           {
               DB::rollBack();
               Log::info('Error in add Client');
               Log::info($ex->getMessage());
           }

           return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Client $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {

        $validator = Validator::make($request->all() , [
            'client_name' => 'required|max:40',
        ]);

        if($validator->fails())
        {
           Session::flash('fail' , "Try Again");
           return redirect()->back();
        }else
        {
           DB::beginTransaction();

           try{
               //changeable
               $updated_client = Client::update_data($client->id ,[
                'client_name' => $request->client_name,
               ]);
                   //update logo for this client
                   //here user might not add any extra image
                   if($request->file('logo') != null )
                   {
                       //delete old image

                        $old_logo_path = substr($client->images[0]->path , strpos($client->images[0]->path , 'assets'));
                        //remove file from assets/images/client
                        File::delete(public_path($old_logo_path));
                        //remove image data from images table
                        $client->images()->delete();

                        //add new logo
                        $image = $request->file('logo');
                        $random_image_name = uniqid().'.'.$image->clientExtension();
                        $image->move(public_path(self::$images_path), $random_image_name);

                        //add image for Images table
                        $image_data = new Image();
                        $image_data->path = env('APP_URL').self::$images_path.$random_image_name;

                        $client->images()->save($image_data);

                   }


               DB::commit();
               Session::flash('success' , 'Done Successfully');
           }catch(\Exception $ex)
           {
               dd($ex->getMessage());
               DB::rollBack();
               Log::info('Error in update client');
               Log::info($ex->getMessage());
           }

           return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        DB::beginTransaction();
        try{
           //here we will delete client with its logo

           //delete images
           foreach($client->images as $image)
           {
               //get image name with extension then delete it from public folder
               $image_name = substr($image->path , strrpos($image->path , '/') +1);
               File::delete(public_path(self::$images_path.$image_name));
               $image->delete();
           }

           $client->delete();
           DB::commit();
           Session::flash('success' , 'Done Successfully');


        }catch(\Exception $ex)
        {
            Session::flash('fail' , "Try Again");
            DB::rollBack();
            Log::info($ex->getMessage());
        }
        return redirect()->back();
    }

    public function deleteGroup(Request $request)
    {
        DB::beginTransaction();

        try{

            unset($request['_token']);
            $deleted_categories_ids = array_values($request->all());

            foreach($deleted_categories_ids as $id)
            {
                //here we will delete logo with its logo
                $client = Client::getById($id);

                //delete images
                foreach($client->images as $image)
                {
                    //get image name with extension then delete it from public folder
                    $image_name = substr($image->path , strrpos($image->path , '/') +1);
                    File::delete(public_path(self::$images_path.$image_name));
                    $image->delete();
                }

                $client->delete();
            }

            DB::commit();
            Session::flash('success' , 'Done Successfully');
        }catch(\Exception $ex)
        {
            Session::flash('fail' , "Try Again");
            DB::rollBack();
            Log::info($ex->getMessage());
        }

        return redirect()->back();
    }
}
