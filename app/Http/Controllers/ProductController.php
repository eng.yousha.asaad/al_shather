<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Image;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{

    public static $images_path =  '/assets/images/product/';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        App::setLocale(Session::get('lang'));
        return view('pages.product.index')->withCategories(Category::getAll())->withProducts(Product::getAll());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all() , [
            'product_name' => 'required|max:40',
            'description' => 'required',
            'category_id' => 'required',
            'images' => 'required'
        ]);

        if($validator->fails())
        {
           Session::flash('fail' , "Try Again");
           return redirect()->back();
        }else
        {
           DB::beginTransaction();

           try{
               $new_product = Product::create([
                'product_name' => $request->product_name,
                'description' => $request->description,
                'category_id' => $request->category_id
               ]);


               if($new_product != null)
               {
                   //add images for this brand
                   foreach($request->file('images') as $image)
                   {
                       $random_image_name = uniqid().'.'.$image->clientExtension();
                       $image->move(public_path(self::$images_path), $random_image_name);

                       //add image for Images table
                       $image_data = new Image();
                       $image_data->path = env('APP_URL').self::$images_path.$random_image_name;

                       $new_product->images()->save($image_data);
                   }

                   //add brand data to translation file
                   storeSetneceInLanguageFile([$request->product_name , $request->description]);

                   DB::commit();
                   Session::flash('success' , 'Done Successfully');
               }
           }catch(\Exception $ex)
           {
               DB::rollBack();
               Log::info('Error in add Product');
               Log::info($ex->getMessage());
           }

           return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $validator = Validator::make($request->all() , [
            'product_name' => 'required|max:40',
            'description' => 'required',
            'category_id' => 'required',
        ]);

        if($validator->fails())
        {
           Session::flash('fail' , "Try Again");
           return redirect()->back();
        }else
        {
           DB::beginTransaction();

           try{

             //chnage sentecnes in language file
             replaceSetneceInLanguageFile([
                $product->product_name => $request->product_name,
                $product->description => $request->description,
               ]);

               //changeable
               $updated_product = Product::update_data($product->id ,[
                'product_name' => $request->product_name,
                'description' => $request->description,
                'category_id' => $request->category_id,
               ]);
                   //add images for this product
                   //here user might not add any extra image
                   if($request->file('images') != null )
                   {

                        foreach($request->file('images') as $image)
                        {
                            $random_image_name = uniqid().'.'.$image->clientExtension();
                            $image->move(public_path(self::$images_path), $random_image_name);

                            //add image for Images table
                            $image_data = new Image();
                            $image_data->path = env('APP_URL').self::$images_path.$random_image_name;

                            $product->images()->save($image_data);
                        }
                   }


               DB::commit();
               Session::flash('success' , 'Done Successfully');
           }catch(\Exception $ex)
           {
               dd($ex->getMessage());
               DB::rollBack();
               Log::info('Error in update brand');
               Log::info($ex->getMessage());
           }

           return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        DB::beginTransaction();
        try{
            $data = [$product->product_name , $product->description];

           //here we will delete brand with its images

           //delete images
           foreach($product->images as $image)
           {
               //get image name with extension then delete it from public folder
               $image_name = substr($image->path , strrpos($image->path , '/') +1);
               File::delete(public_path(self::$images_path.$image_name));
               $image->delete();
           }

           $product->delete();
           deleteSetneceInLanguageFile($data);

           DB::commit();
           Session::flash('success' , 'Done Successfully');


        }catch(\Exception $ex)
        {
            Session::flash('fail' , "Try Again");
            DB::rollBack();
            Log::info($ex->getMessage());
        }
        return redirect()->back();
    }

    public function deleteGroup(Request $request)
    {
        DB::beginTransaction();

        try{

            unset($request['_token']);
            $deleted_products_ids = array_values($request->all());

            foreach($deleted_products_ids as $id)
            {
                //here we will delete brand with its images
                $product = Product::getById($id);

                //delete images
                foreach($product->images as $image)
                {
                    //get image name with extension then delete it from public folder
                    $image_name = substr($image->path , strrpos($image->path , '/') +1);
                    File::delete(public_path(self::$images_path.$image_name));
                    $image->delete();
                }

                $product->delete();
            }

            DB::commit();
            Session::flash('success' , 'Done Successfully');
        }catch(\Exception $ex)
        {
            Session::flash('fail' , "Try Again");
            DB::rollBack();
            Log::info($ex->getMessage());
        }

        return redirect()->back();
    }
}
