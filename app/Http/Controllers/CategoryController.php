<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{

    public static $images_path =  '/assets/images/category/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        App::setLocale(Session::get('lang'));
        return view('pages.category.index')->withCategories(Category::getAll())->withBrands(Brand::getAll());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all() , [
            'category_name' => 'required|max:40',
            'description' => 'required',
            'brand_id' => 'required',
            'images' => 'required'
        ]);

        if($validator->fails())
        {
           Session::flash('fail' , "Try Again");
           return redirect()->back();
        }else
        {
           DB::beginTransaction();

           try{
               $new_category = Category::create([
                'category_name' => $request->category_name,
                'description' => $request->description,
                'brand_id' => $request->brand_id
               ]);


               if($new_category != null)
               {
                   //add images for this category
                   foreach($request->file('images') as $image)
                   {
                       $random_image_name = uniqid().'.'.$image->clientExtension();
                       $image->move(public_path(self::$images_path), $random_image_name);

                       //add image for Images table
                       $image_data = new Image();
                       $image_data->path = env('APP_URL').self::$images_path.$random_image_name;

                       $new_category->images()->save($image_data);
                   }

                   //add brand data to translation file
                   storeSetneceInLanguageFile([$request->category_name , $request->description]);

                   DB::commit();
                   Session::flash('success' , 'Done Successfully');
               }
           }catch(\Exception $ex)
           {
               DB::rollBack();
               Log::info('Error in add Category');
               Log::info($ex->getMessage());
           }

           return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {

        $validator = Validator::make($request->all() , [
            'category_name' => 'required|max:40',
            'description' => 'required',
            'brand_id' => 'required',
        ]);

        if($validator->fails())
        {
           Session::flash('fail' , "Try Again");
           return redirect()->back();
        }else
        {
           DB::beginTransaction();

           try{
                //chnage sentecnes in language file
                replaceSetneceInLanguageFile([
                    $category->category_name => $request->category_name,
                    $category->description => $request->description,
                   ]);

               //changeable
               $updated_category = Category::update_data($category->id ,[
                'category_name' => $request->category_name,
                'description' => $request->description,
                'brand_id' => $request->brand_id,
               ]);
                   //add images for this category
                   //here user might not add any extra image
                   if($request->file('images') != null )
                   {

                        foreach($request->file('images') as $image)
                        {
                            $random_image_name = uniqid().'.'.$image->clientExtension();
                            $image->move(public_path(self::$images_path), $random_image_name);

                            //add image for Images table
                            $image_data = new Image();
                            $image_data->path = env('APP_URL').self::$images_path.$random_image_name;

                            $category->images()->save($image_data);
                        }
                   }


               DB::commit();
               Session::flash('success' , 'Done Successfully');
           }catch(\Exception $ex)
           {
               dd($ex->getMessage());
               DB::rollBack();
               Log::info('Error in update category');
               Log::info($ex->getMessage());
           }

           return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        DB::beginTransaction();
        try{
            $data = [$category->category_name , $category->description];

           //here we will delete category with its images

           //delete images
           foreach($category->images as $image)
           {
               //get image name with extension then delete it from public folder
               $image_name = substr($image->path , strrpos($image->path , '/') +1);
               File::delete(public_path(self::$images_path.$image_name));
               $image->delete();
           }

           $category->delete();
           deleteSetneceInLanguageFile($data);
           DB::commit();
           Session::flash('success' , 'Done Successfully');


        }catch(\Exception $ex)
        {
            Session::flash('fail' , "Try Again");
            DB::rollBack();
            Log::info($ex->getMessage());
        }
        return redirect()->back();
    }

    public function deleteGroup(Request $request)
    {
        DB::beginTransaction();

        try{

            unset($request['_token']);
            $deleted_categories_ids = array_values($request->all());

            foreach($deleted_categories_ids as $id)
            {
                //here we will delete category with its images
                $category = Category::getById($id);

                //delete images
                foreach($category->images as $image)
                {
                    //get image name with extension then delete it from public folder
                    $image_name = substr($image->path , strrpos($image->path , '/') +1);
                    File::delete(public_path(self::$images_path.$image_name));
                    $image->delete();
                }

                $category->delete();
            }

            DB::commit();
            Session::flash('success' , 'Done Successfully');
        }catch(\Exception $ex)
        {
            Session::flash('fail' , "Try Again");
            DB::rollBack();
            Log::info($ex->getMessage());
        }

        return redirect()->back();
    }

}
