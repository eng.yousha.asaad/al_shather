<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Brand;
use App\Models\Client;
use App\Models\Project;
use Illuminate\Contracts\Session\Session as SessionSession;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        if(!Session::has('lang'))
        {
            Session::put('lang' , 'en');
        }
        App::setLocale(Session::get('lang'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       // $request->visitor()->visit();
        App::setLocale(Session::get('lang'));
       try{
        return view('pages.website.home')
        ->withBrands(Brand::getAll())
        ->withProjects(Project::getAll())
        ->withActivities(Activity::getAll())
        ->withClients(Client::getAll());
       }catch(\Exception $ex)
       {
           dd($ex->getMessage());
       }

    }
}
