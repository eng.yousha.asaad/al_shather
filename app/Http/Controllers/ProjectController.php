<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      App::setLocale(Session::get('lang'));
       return view('pages.project.index')->withProjects(Project::getAll());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all() , [
            'project_name' => 'required|max:40',
            'description' => 'required',
            'images' => 'required'
        ]);

        if($validator->fails())
        {
           Session::flash('fail' , "Try Again");
           return redirect()->back();
        }else
        {
           DB::beginTransaction();

           try{
               $new_project = Project::create([
                'project_name' => $request->project_name,
                'description' => $request->description,
               ]);


               if($new_project != null)
               {
                   //add images for this project
                   foreach($request->file('images') as $image)
                   {
                       $random_image_name = uniqid().'.'.$image->clientExtension();
                       $image->move(public_path('assets/images/project'), $random_image_name);

                       //add image for Images table
                       $image_data = new Image();
                       $image_data->path = env('APP_URL').'/assets/images/project/'.$random_image_name;

                       $new_project->images()->save($image_data);
                   }
                   //add brand data to translation file
                   storeSetneceInLanguageFile([$request->project_name , $request->description]);

                   DB::commit();
                   Session::flash('success' , 'Done Successfully');
               }
           }catch(\Exception $ex)
           {
               DB::rollBack();
               Log::info('Error in add project');
               Log::info($ex->getMessage());
           }

           return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $validator = Validator::make($request->all() , [
            'project_name' => 'required|max:40',
            'description' => 'required',
        ]);

        if($validator->fails())
        {
           Session::flash('fail' , "Try Again");
           return redirect()->back();
        }else
        {
           DB::beginTransaction();

           try{
              //chnage sentecnes in language file
              replaceSetneceInLanguageFile([
                $project->project_name => $request->project_name,
                $project->description => $request->description,
               ]);


               //changeable
               $updated_project = Project::update_data($project->id ,[
                'project_name' => $request->project_name,
                'description' => $request->description,
               ]);
                   //add images for this project
                   //here user might not add any extra image
                   if($request->file('images') != null )
                   {

                        foreach($request->file('images') as $image)
                        {
                            $random_image_name = uniqid().'.'.$image->clientExtension();
                            $image->move(public_path('assets/images/project'), $random_image_name);

                            //add image for Images table
                            $image_data = new Image();
                            $image_data->path = env('APP_URL').'/assets/images/project/'.$random_image_name;

                            $project->images()->save($image_data);
                        }
                   }


               DB::commit();
               Session::flash('success' , 'Done Successfully');
           }catch(\Exception $ex)
           {
               dd($ex->getMessage());
               DB::rollBack();
               Log::info('Error in update project');
               Log::info($ex->getMessage());
           }

           return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        DB::beginTransaction();
        try{
            $data = [$project->project_name , $project->description];
           //here we will delete project with its images

           //delete images
           foreach($project->images as $image)
           {
               //get image name with extension then delete it from public folder
               $image_name = substr($image->path , strrpos($image->path , '/') +1);
               File::delete(public_path('assets/images/project/'.$image_name));
               $image->delete();
           }

           $project->delete();
           deleteSetneceInLanguageFile($data);
           DB::commit();
           Session::flash('success' , 'Done Successfully');


        }catch(\Exception $ex)
        {
            Session::flash('fail' , "Try Again");
            DB::rollBack();
            Log::info($ex->getMessage());
        }

        return redirect()->back();
    }

    public function deleteGroup(Request $request)
    {
        DB::beginTransaction();
        try{
            unset($request['_token']);
            $deleted_projects_ids = array_values($request->all());
            foreach($deleted_projects_ids as $id)
            {
                //here we will delete project with its images
                $project = Project::getById($id);


                //delete images
                foreach($project->images as $image)
                {
                    //get image name with extension then delete it from public folder
                    $image_name = substr($image->path , strrpos($image->path , '/') +1);
                    File::delete(public_path('assets/images/project/'.$image_name));
                    $image->delete();
                }

                $project->delete();

            }

            DB::commit();
            Session::flash('success' , 'Done Successfully');


        }catch(\Exception $ex)
        {
            Session::flash('fail' , "Try Again");
            DB::rollBack();
            Log::info($ex->getMessage());
        }

        return redirect()->back();
    }

}
