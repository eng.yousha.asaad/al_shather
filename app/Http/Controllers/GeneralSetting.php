<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session as FacadesSession;
use Illuminate\Support\Facades\Validator;
use Session;

class GeneralSetting extends Controller
{
    public function index(){
        App::setLocale(Session::get('lang'));
        $critical_time = env('CRITICAL_TIME');
        $warning_time = env('WARNING_TIME');

        return view('pages.general_setting.index')->with(compact('critical_time' , 'warning_time'));
    }

    public function update_times(Request $request)
    {
        $validator = Validator::make($request->all() , [
            'critical_time' => 'required',
            'warning_time' => 'required',
        ]);

        if($validator->fails())
        {
           Session::flash('fail' , "Try Again");
           return redirect()->back();
        }else
        {
            $this->setEnv('CRITICAL_TIME' , $request->critical_time);
            $this->setEnv('WARNING_TIME' , $request->warning_time);
            Session::flash('success' , 'Done Successfully');
            return redirect()->back();

        }
    }


    function setEnv($name, $value)
    {
        $path = base_path('.env');
        if (file_exists($path)) {
            file_put_contents($path, str_replace(
                $name . '=' . env($name), $name . '=' . $value, file_get_contents($path)
            ));
        }
    }

    public function change_language(Request $request)
    {
        App::setLocale(Session::get('lang'));
        FacadesSession::put('lang' , $request->lang);
        return redirect()->back();
    }
}
