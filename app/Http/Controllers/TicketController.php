<?php

namespace App\Http\Controllers;

use App\Models\Ticket;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class TicketController extends Controller
{

    public function __construct()
    {

      $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        App::setLocale(Session::get('lang'));

        $critical = env('CRITICAL_TIME');
        $warning = env('WARNING_TIME');


        $warning_date = Carbon::now()->subDays($warning);
        $critical_date = Carbon::now()->subDays($critical);

        if($request->ticket_type == 'warning')
        {
            $tickets = Ticket::whereDate( 'created_at', '<=' , $warning_date)
                               ->where('solved',false)->get();
        }else
            if($request->ticket_type == 'critical')
            {
                $tickets = Ticket::whereDate( 'created_at', '<=' , $critical_date )
                                   ->whereDate('created_at','>', $warning_date)
                                   ->where('solved',false)
                                   ->get();

            }else
                if($request->ticket_type == 'solved')
                {
                    $tickets = Ticket::where('solved',true)->get();
                }else
                    if($request->ticket_type == 'new')
                    {
                        $tickets = Ticket::whereDate( 'created_at', '>' , $critical_date )
                                          ->where('solved',false)->get();
                    }else
                    {
                        $tickets = Ticket::getAll();
                    }

        return view('pages.ticket.index')->with(compact('tickets' , 'warning' , 'critical'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        App::setlocale(Session::get('lang'));
        return view('pages.website.create_ticket');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all() , [
            'subject' => 'required',
            'description' => 'required',
        ]);

        if($validator->fails())
        {
           Session::flash('fail' , "Try Again");
           return redirect()->back();
        }else
        {
           DB::beginTransaction();

           try{

               $file_path = null;

               if($request->has('file'))
                {
                    $file = $request->file('file');
                    $random_file_name = uniqid().'.'.$file->clientExtension();
                    $file_path = 'assets/upload/ticket/'.$random_file_name;
                    $file->move(public_path('assets/upload/ticket'), $random_file_name);
                }

               $new_ticket = Ticket::create([
                'subject' => $request->subject,
                'description' => $request->description,
                'file_path' => $file_path,
                'user_id' => auth()->user()->id,
               ]);

               if($new_ticket != null)
               {
                   DB::commit();
                   Session::flash('success' , 'Done Successfully');
               }
           }catch(\Exception $ex)
           {
               DB::rollBack();
               Log::info('Error in add Ticket');
               Log::info($ex->getMessage());
           }

           return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        dd('show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        dd('edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        DB::beginTransaction();
        try{
            $ticket->solved = true;
            $ticket->save();
            DB::commit();
            Session::flash('success' , 'Done Successfully');
        }catch(\Exception $ex)
        {
            DB::rollBack();
            Session::flash('fail' , "Try Again");
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        dd('destory');
    }

    public function resolve_all_tickets(Request $request)
    {
        DB::beginTransaction();
        try{
            unset($request['_token']);
            $resolved_tickets_ids = array_values($request->all());

            foreach($resolved_tickets_ids as $id)
            {
                //here we will resolve ticket with its images
                $ticket = Ticket::getById($id);
                $ticket->solved = true;
                $ticket->save();
            }


            DB::commit();
            Session::flash('success' , 'Done Successfully');

        }catch(\Exception $ex)
        {
            Session::flash('fail' , "Try Again");
            DB::rollBack();
            Log::info($ex->getMessage());
        }

        return redirect()->back();
    }
}
