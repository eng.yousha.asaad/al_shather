<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File as FileStore;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class FileController extends Controller
{


    public function __construct()
    {
      $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       App::setLocale(Session::get('lang'));
       return view('pages.files.index')->withFiles(File::getAll());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all() , [
            'file_name' => 'required|max:40',
            'type' =>'required',
            'description' => 'required',
            'file' => 'required'
        ]);

        if($validator->fails())
        {
           Session::flash('fail' , "Try Again");
           return redirect()->back();
        }else
        {
           DB::beginTransaction();

           try{

                $file_path = null;
                if($request->has('file'))
                {
                    $file_path = $this->storeFile($request->file('file'));
                }

               $new_file = File::create([
                'file_name' => $request->file_name,
                'description' => $request->description,
                'type' => $request->type,
                'file_path' => $file_path
               ]);

               //add brand data to translation file
               storeSetneceInLanguageFile([$request->file_name , $request->description]);
               DB::commit();
               Session::flash('success' , 'Done Successfully');
           }catch(\Exception $ex)
           {
               DB::rollBack();
               Log::info('Error in add File');
               Log::info($ex->getMessage());
           }

           return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\File  $file
     * @return \Illuminate\Http\Response
     */
    public function show(File $file)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\File  $file
     * @return \Illuminate\Http\Response
     */
    public function edit(File $file)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\File  $file
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, File $file)
    {
        $validator = Validator::make($request->all() , [
            'file_name' => 'required|max:40',
            'type' => 'required',
            'description' => 'required',
        ]);

        if($validator->fails())
        {
           Session::flash('fail' , "Try Again");
           return redirect()->back();
        }else
        {
           DB::beginTransaction();

           try{

             //chnage sentecnes in language file
             replaceSetneceInLanguageFile([
                $file->file_name => $request->file_name,
                $file->description => $request->description,
            ]);

            $file_path = $file->file_path;
            if($request->has('file'))
            {
                //delete old file
                $oldFile = substr( $file_path, strpos($file_path , 'assets'));
                FileStore::delete(public_path($oldFile));

                //store new file
                $file_path = $this->storeFile($request->file('file'));
            }

            //update data
             File::update_data($file->id ,[
                'file_name' => $request->file_name,
                'type' => $request->type,
                'description' => $request->description,
                'file_path' => $file_path
               ]);

               DB::commit();
               Session::flash('success' , 'Done Successfully');
           }catch(\Exception $ex)
           {
               dd($ex->getMessage());
               DB::rollBack();
               Log::info('Error in update File');
               Log::info($ex->getMessage());
           }

           return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\File  $file
     * @return \Illuminate\Http\Response
     */
    public function destroy(File $file)
    {
        DB::beginTransaction();
        try{

           //here we will delete file with its images

           $data = [$file->file_name , $file->description];

           //delete file from assets/images/upload
           if($file->file_path != null)
           {
               $file_path = substr($file->file_path , strpos($file->file_path , 'asset'));
               FileStore::delete(public_path($file_path));
           }

           $file->delete();
           deleteSetneceInLanguageFile($data);
           DB::commit();
           Session::flash('success' , 'Done Successfully');


        }catch(\Exception $ex)
        {
            Session::flash('fail' , "Try Again");
            DB::rollBack();
            Log::info($ex->getMessage());
        }

        return redirect()->back();
    }

    public function deleteGroup(Request $request)
    {
        DB::beginTransaction();
        try{
            unset($request['_token']);
            $deleted_files_ids = array_values($request->all());
            foreach($deleted_files_ids as $id)
            {
                //here we will delete file with its images
                $file = File::getById($id);

               //delete file from assets/images/upload
                if($file->file_path != null)
                {
                    $file_path = substr($file->file_path , strpos($file->file_path , 'asset'));
                    FileStore::delete(public_path($file_path));
                }

                $file->delete();
            }

            DB::commit();
            Session::flash('success' , 'Done Successfully');


        }catch(\Exception $ex)
        {
            Session::flash('fail' , "Try Again");
            DB::rollBack();
            Log::info($ex->getMessage());
        }

        return redirect()->back();
    }


    public function storeFile($file){
        $random_file_name = uniqid().'.'.$file->clientExtension();
        $file->move(public_path('assets/images/upload'), $random_file_name);

        $file_path = env('APP_URL').'/assets/images/upload/'.$random_file_name;
        return $file_path;
    }


    public function show_downloads($type){

        App::setLocale(Session::get('lang'));
        if($type == 'all')
        {
            $downloads = File::getAll();
        }else
        {
            $downloads = File::getByColumn('type' , $type);
        }

        return view('pages.website.downloads')->withDownloads($downloads);
    }
}
