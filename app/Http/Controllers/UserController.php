<?php

namespace App\Http\Controllers;

use App\Models\User;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        App::setLocale(Session::get('lang'));
        return view('pages.users.requests');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => ['required'],
            'company' => ['required' , 'string' , 'min:1'],
            'role'  => ['required' , 'string']
        ]);

        if($validator->fails())
        {
            $error =[];
            foreach($validator->getMessageBag()->getMessages() as $key => $value)
            {
                $error[]= $key.': '.join(" , " , $value);
            }

            Session::flash('fail' , join("\n" , $error));
        }else{

            try{
                $user_data=[
                    'name' => $request->name,
                    'email' => $request->email,
                    'phone' => Crypt::encrypt($request->phone),
                    'company' => $request->company,
                    'password' => Hash::make($request->password),
                    'role' => $request->role,
                    'verified' => true,
                ];

                User::create($user_data);
                DB::commit();
                Session::flash('success' , 'Done Successfully');
            }catch(\Exception $ex)
            {
                Session::flash('fail' , "Try Again");
                DB::rollBack();
                Log::info($ex->getMessage());
            }

        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,User $user)
    {
       DB::beginTransaction();

       try{
           $user->verified = true;
           $user->save();
           DB::commit();
           Session::flash('success' , 'Done Successfully');
       }catch(\Exception $ex)
       {
            Session::flash('fail' , "Try Again");
            DB::rollBack();
            Log::info($ex->getMessage());
       }
       return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verifyGroup(Request $request)
   {
       DB::beginTransaction();
       try{
           unset($request['_token']);
           $users_ids = array_values($request->all());
           foreach($users_ids as $id)
           {
               //here we will delete activity with its images
               $user = User::getById($id);
               $user->verified = true;
               $user->save();
           }

           DB::commit();
           Session::flash('success' , 'Done Successfully');


       }catch(\Exception $ex)
       {
           Session::flash('fail' , "Try Again");
           DB::rollBack();
           Log::info($ex->getMessage());
       }

       return redirect()->back();
   }

}
