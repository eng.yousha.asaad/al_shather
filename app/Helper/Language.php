<?php

use Illuminate\Support\Facades\Session;

     function translate($sentence)
    {
       if(Session::get('lang') == 'en')
       {
           return $sentence;
       }else
       {
           return trans('sentence.'.$sentence);
       }
    }

   function storeSetneceInLanguageFile($data)
    {
        //get array that contain all english sentences from file that contain
        // all sentences in english language
        $sentences = require resource_path('lang/ar/sentence.php');

        //add this sentence into previous array
        //and make the key and value has the same value
        foreach($data as $new_sentnce)
        {
            $sentences[$new_sentnce] = $new_sentnce;
        }


        //then we store array in sentence language file
        file_put_contents(resource_path('lang/ar/sentence.php'), "<?php \n return " . var_export($sentences, true) . ";");

        return true;
    }

    function replaceSetneceInLanguageFile($data)
    {
        //get array that contain all english sentences from file that contain
        // all sentences in english language
        $sentences = require resource_path('lang/ar/sentence.php');

        //add this sentence into previous array
        //and make the key and value has the same value
        foreach($data as $old_sentence => $new_sentnce)
        {
            //here user has updated sentence
            if($old_sentence != $new_sentnce)
            {
                unset($sentences[$old_sentence]);
                $sentences[$new_sentnce] = $new_sentnce;
            }
        }


        //then we store array in sentence language file
        file_put_contents(resource_path('lang/ar/sentence.php'), "<?php \n return " . var_export($sentences, true) . ";");

        return true;
    }

    function deleteSetneceInLanguageFile($data)
    {
        //get array that contain all english sentences from file that contain
        // all sentences in english language
        $sentences = require resource_path('lang/ar/sentence.php');

        //add this sentence into previous array
        //and make the key and value has the same value
        foreach($data as $deleted_sentence )
        {
            unset($sentences[$deleted_sentence]);
        }

        //then we store array in sentence language file
        file_put_contents(resource_path('lang/ar/sentence.php'), "<?php \n return " . var_export($sentences, true) . ";");

        return true;
    }

