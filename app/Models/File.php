<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class File extends Model implements Operations
{
    use HasFactory;

    public $fillable = ['file_name' , 'description' , 'type','file_path'];


    //operation
    public static function getAll(){
        return File::all();
    }

    public static function  getById($id){
        return File::find($id);
    }

    public static function getByColumn($column , $value){
        return DB::table('files')->where($column , $value)->get();
    }

    public static function insert_data($data){
        return DB::table('files')->insert($data);
    }

    public static function update_data($id , $data){
       return  DB::table('files')->where('id' , $id)->update($data);
    }
    public static function delete_data($id){
        return DB::table('files')->where('id' , $id)->delete();
    }

}

