<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Ticket extends Model implements Operations
{
    use HasFactory;

    public $fillable = ['subject' , 'description' , 'user_id' , 'file_path'];

    //relations

    public function user(){
        return $this->belongsTo(User::class);
    }



    //operation
    public static function getAll(){
        return Ticket::all();
    }

    public static function  getById($id){
        return Ticket::find($id);
    }

    public static function getByColumn($column , $value){
        return DB::table('tickets')->where($column , $value)->get();
    }

    public static function insert_data($data){
        return DB::table('tickets')->insert($data);
    }

    public static function update_data($id , $data){
       return  DB::table('tickets')->where('id' , $id)->update($data);
    }
    public static function delete_data($id){
        return DB::table('tickets')->where('id' , $id)->delete();
    }

}
