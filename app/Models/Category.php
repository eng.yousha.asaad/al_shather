<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model implements Operations
{
    use HasFactory;
    public $fillable = ['category_name' , 'description' , 'brand_id'];
    public $timestamps = false;

    public function brand(){
        return $this->belongsTo(Brand::class);
    }

    public function images(){
        return $this->morphMany(Image::class , 'imageable');
    }
    public function products(){
        return $this->hasMany(Product::class);
    }


        //operation
        public static function getAll(){
            return Category::all();
        }

        public static function  getById($id){
            return Category::find($id);
        }

        public static function getByColumn($column , $value){
            return DB::table('categories')->where($column , $value)->get();
        }

        public static function insert_data($data){
            return DB::table('categories')->insert($data);
        }

        public static function update_data($id , $data){
           return  DB::table('categories')->where('id' , $id)->update($data);
        }
        public static function delete_data($id){
            return DB::table('categories')->where('id' , $id)->delete();
        }

        public static function getCategoriesForBrand($brand_id)
        {
            return Category::where('brand_id' , $brand_id)->get();
        }

}
