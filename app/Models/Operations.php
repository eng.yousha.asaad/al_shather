<?php
namespace App\Models;

interface Operations{
    public static function getAll();
    public static function  getById($id);
    public static function getByColumn($column , $value);

    public static function insert_data($data);
    public static function update_data($id , $data);
    public static function delete_data($id);

}
