<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SubActivity extends Model implements Operations
{
    use HasFactory;
    public $fillable = ['sub_activity_name' , 'description' , 'activity_id'];
    public $timestamps = false;


    public function activity(){
        return $this->belongsTo(Activity::class);
    }

    public function images(){
        return $this->morphMany(Image::class , 'imageable');
    }



        //operation
        public static function getAll(){
            return SubActivity::all();
        }

        public static function  getById($id){
            return SubActivity::find($id);
        }

        public static function getByColumn($column , $value){
            return DB::table('sub_activities')->where($column , $value)->get();
        }

        public static function insert_data($data){
            return DB::table('sub_activities')->insert($data);
        }

        public static function update_data($id , $data){
           return  DB::table('sub_activities')->where('id' , $id)->update($data);
        }
        public static function delete_data($id){
            return DB::table('sub_activities')->where('id' , $id)->delete();
        }

        public static function getSubActivitiesForActivity($activity_id)
        {
            return SubActivity::where('activity_id' , $activity_id)->get();
        }
}
