<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model implements Operations
{
    use HasFactory;
    public $fillable = ['product_name' , 'description' , 'category_id'];
    public $timestamps = false;

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function images(){
        return $this->morphMany(Image::class , 'imageable');
    }

        //operation
        public static function getAll(){
            return Product::all();
        }

        public static function  getById($id){
            return Product::find($id);
        }

        public static function getByColumn($column , $value){
            return DB::table('products')->where($column , $value)->get();
        }

        public static function insert_data($data){
            return DB::table('products')->insert($data);
        }

        public static function update_data($id , $data){
           return  DB::table('products')->where('id' , $id)->update($data);
        }
        public static function delete_data($id){
            return DB::table('products')->where('id' , $id)->delete();
        }
}
