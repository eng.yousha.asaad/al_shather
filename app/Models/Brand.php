<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Brand extends Model implements Operations
{
    use HasFactory;

    public $fillable = ['brand_name' , 'description'];
    public $timestamps = false;

    //relations

    public function categories(){
        return $this->hasMany(Category::class);
    }

    public function images(){
        return $this->morphMany(Image::class , 'imageable');
    }

    //operation
    public static function getAll(){
        return Brand::all();
    }

    public static function  getById($id){
        return Brand::find($id);
    }

    public static function getByColumn($column , $value){
        return DB::table('brands')->where($column , $value)->get();
    }

    public static function insert_data($data){
        return DB::table('brands')->insert($data);
    }

    public static function update_data($id , $data){
       return  DB::table('brands')->where('id' , $id)->update($data);
    }
    public static function delete_data($id){
        return DB::table('brands')->where('id' , $id)->delete();
    }

}
