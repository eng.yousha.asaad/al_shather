<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements Operations
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'password',
        'company',
        'role',
        'verified',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

     //operation
     public static function getAll(){
        return User::all();
    }

    public static function  getById($id){
        return User::find($id);
    }

    public static function getByColumn($column , $value){
        return DB::table('users')->where($column , $value)->get();
    }

    public static function insert_data($data){
        return DB::table('users')->insert($data);
    }

    public static function update_data($id , $data){
       return  DB::table('users')->where('id' , $id)->update($data);
    }
    public static function delete_data($id){
        return DB::table('users')->where('id' , $id)->delete();
    }
}
