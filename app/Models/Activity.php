<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Activity extends Model implements Operations
{
    use HasFactory;

    public $fillable = ['activity_name' , 'description'];
    public $timestamps = false;

    //relations

    public function images(){
        return $this->morphMany(Image::class , 'imageable');
    }

    //operation
    public static function getAll(){
        return Activity::all();
    }

    public static function  getById($id){
        return Activity::find($id);
    }

    public static function getByColumn($column , $value){
        return DB::table('activities')->where($column , $value)->get();
    }

    public static function insert_data($data){
        return DB::table('activities')->insert($data);
    }

    public static function update_data($id , $data){
       return  DB::table('activities')->where('id' , $id)->update($data);
    }
    public static function delete_data($id){
        return DB::table('activities')->where('id' , $id)->delete();
    }
}
