@extends('pages.index')

@section('page_title')
Sentences
@endsection

@section('styles')
<style>

    a{
        cursor: pointer;
    }
    .image-item{
        position: relative;
        max-width: 150px;
        width: 150px;
        height: 80px;
        margin: 8px;
        border-radius: 10px;
        overflow: hidden;
        cursor: pointer;
    }

    .image-item  img{
        width: 100%;
        max-height: 80px;
        display: block;
    }

    .overlay{
        width: 100%;
        height:100%;
        position: absolute;
        top:0;
        opacity:0;
        background-color: #fff;
        display: flex;
        justify-content: center;
        align-items: center;
    }




    .image-item:hover  .overlay{
        opacity: 0.5;
    }

    .image-box{
        display: flex;
        justify-content: flex-start;
        flex-wrap: wrap;
        width:100%;
    }

   .delete-image-btn{
       background-color: transparent;
       border: none;
       color: red;
   }

   .preview-image-btn > i{
       font-size: 25px !important;
       color: blue !important;
    }
</style>

<script>
    $(document).ready(function(){
        // Activate tooltip
        $('[data-toggle="tooltip"]').tooltip();

        // Select/Deselect checkboxes
        var checkbox = $('table tbody input[type="checkbox"]');
        $("#selectAll").click(function(){
            if(this.checked){
                checkbox.each(function(){
                    this.checked = true;
                });
            } else{
                checkbox.each(function(){
                    this.checked = false;
                });
            }
        });
        checkbox.click(function(){
            if(!this.checked){
                $("#selectAll").prop("checked", false);
            }
        });
    });
    </script>


@endsection

@section('content')
<div class="container-fluid">
	<div class="table-responsive">
		<div class="table-wrapper">
			<div class="table-title">

                <div class="row">

                    <div class="col-sm-6">
						<h2>Manage <b>Translation</b></h2>
					</div>

                    <div class="col-sm-6">
						<button id="translate_btn"  class="btn btn-success"><i class="material-icons">&#xE147;</i> <span>Translate</span></button>
					</div>

                </div>
			</div>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>English</th>
                        <th>Arabic Translation</th>
					</tr>
				</thead>
				<tbody>
                    <form id="translation_form" action="{{route('sentence.update' ,[1])}}" method="post">
                        @csrf
                        @method('PUT')
                        @foreach ($sentences as $english => $arabic)
                            <tr>
                                <td width="50%">{{str_replace('_' ,' ' ,$english)}}</td>
                                <td width="50%">
                                    <textarea name="{{$english}}"  cols="30" rows="2" dir="rtl" style="padding: 5px" class="form-control">{{$arabic}}</textarea>
                                </td>
                            </tr>
                        @endforeach
                    </form>
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>

    $('#translate_btn').click(function(){
        $('#translation_form').submit();
    });



</script>
@endsection



