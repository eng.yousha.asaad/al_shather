@extends('pages.index')

@section('page_title')
Sub Activities
@endsection

@section('styles')
<style>

    a{
        cursor: pointer;
    }
    .image-item{
        max-width: 150px;
        width: 150px;
        height: 80px;
        margin: 8px;
        border-radius: 10px;
        overflow: hidden;
        cursor: pointer;
    }

    .image-item  img{
        max-width: 100%;
        height: auto;
        object-fit: cover;
        display: block;
    }

    .overlay{
        width: 100%;
        height:100%;
        position: absolute;
        top:0;
        opacity:0;
        background-color: #fff;
        display: flex;
        justify-content: center;
        align-items: center;
    }




    .image-item:hover  .overlay{
        opacity: 0.5;
    }

    .image-box{
        display: flex;
        justify-content: flex-start;
        flex-wrap: wrap;
        width:100%;
    }

   .delete-image-btn{
       background-color: transparent;
       border: none;
       color: red;
   }

   .preview-image-btn > i{
       font-size: 25px !important;
       color: blue !important;
    }
</style>

<script>
    $(document).ready(function(){
        // Activate tooltip
        $('[data-toggle="tooltip"]').tooltip();

        // Select/Deselect checkboxes
        var checkbox = $('table tbody input[type="checkbox"]');
        $("#selectAll").click(function(){
            if(this.checked){
                checkbox.each(function(){
                    this.checked = true;
                });
            } else{
                checkbox.each(function(){
                    this.checked = false;
                });
            }
        });
        checkbox.click(function(){
            if(!this.checked){
                $("#selectAll").prop("checked", false);
            }
        });
    });
    </script>


@endsection

@section('content')
<div class="container-fluid">
	<div class="table-responsive">
		<div class="table-wrapper">
			<div class="table-title">
                <div class="row">

                    <div class="col-sm-6">
						<h2>Manage <b>Sub Activities</b></h2>
					</div>

                    <div class="col-sm-6">
						<button id="add_modal_btn"     class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Add New Sub Activity</span></button>
						<button id="delete_sub_activities_btn" class="btn btn-danger" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Delete</span></button>
					</div>

                </div>
			</div>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>
							<span class="custom-checkbox">
								<input type="checkbox" id="selectAll">
								<label for="selectAll"></label>
							</span>
						</th>
						<th>Name</th>
						<th>Description</th>
                        <th>Main Activty</th>
                        <th>images</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
				  @if(count($subactivities) > 0)
                      @foreach ($subactivities as $sub_activity)
                        <tr>
                            <td width="10%">
                                <span class="custom-checkbox">
                                    <input type="checkbox" id="checkbox{{$sub_activity->id}}" name="options[]" value="{{$sub_activity->id}}">
                                    <label for="checkbox{{$sub_activity->id}}"></label>
                                </span>
                            </td>
                            <td width="15%">{{$sub_activity->sub_activity_name}}</td>
                            <td width="15%">{{$sub_activity->description}}</td>
                            <td width="15%" id="{{$sub_activity->activity->id}}">{{$sub_activity->activity->activity_name}}</td>
                            <td width="35%" >
                                <div class="image-box">
                                    @if(count($sub_activity->images) > 0)
                                        @foreach ( $sub_activity->images as $image)
                                            <div class="image-item" style="position: relative">
                                                <img src="{{$image->path}}" alt="error im image">
                                                <div class="overlay">
                                                    <a  class="preview-image-btn" href="{{$image->path}}" target="_blank">
                                                        <i class="material-icons" data-toggle="tooltip" title="Preview image">&#xe417;</i>
                                                    </a>

                                                    <form  method="post" action="{{route('image.destroy' , [$image->id])}}" >
                                                        @csrf
                                                        @method('delete')
                                                        <button type="submit" class="material-icons delete-image-btn" data-toggle="tooltip" title="Delete image">&#xE872;</button>
                                                    </form>

                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                      <div>
                                          No images avilable for this Sub Activity
                                      </div>
                                    @endif
                                </div>
                            </td>
                            <td width="10%">
                                <a class="edit"  data-sub_activity_id="{{$sub_activity->id}}" data-toggle="modal">
                                    <i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i>
                                </a>
                                <a class="delete" data-toggle="modal">
                                <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                                <form method="post" action="sub-activity/{{$sub_activity->id}}">
                                    @method('delete')
                                    @csrf
                                </form>
                            </td>
                        </tr>
                      @endforeach
                  @else
                     <tr style="text-align: center">
                         <td colspan="6">No Data Avilable</td>
                     </tr>
                  @endif

				</tbody>
			</table>
        <!--
			<div class="clearfix">
				<div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>
				<ul class="pagination">
					<li class="page-item disabled"><a href="#">Previous</a></li>
					<li class="page-item"><a href="#" class="page-link">1</a></li>
					<li class="page-item"><a href="#" class="page-link">2</a></li>
					<li class="page-item active"><a href="#" class="page-link">3</a></li>
					<li class="page-item"><a href="#" class="page-link">4</a></li>
					<li class="page-item"><a href="#" class="page-link">5</a></li>
					<li class="page-item"><a href="#" class="page-link">Next</a></li>
				</ul>
			</div>
        -->
		</div>
	</div>
</div>
<!-- Add Modal HTML -->
@include('modals.sub_activity.addSubActivity')
<!-- Edit Modal HTML -->
@include('modals.sub_activity.editSubActivity')
<!-- Delete Modal HTML -->
@include('modals.sub_activity.deleteSubActivity')

<form action="delete_sub_activities"  method="post" id="delete_sub_activities_form">

</form>
@endsection

@section('scripts')

<script>
    $(document).ready(function(){

        $('#add_modal_btn').click(function(){
            $('#addSubActivityModal').modal('show');
        });

        $('.edit').click(function(){

            let sub_activity = $(this).parent().parent().children();

            let c_name = sub_activity[1].textContent;
            let c_description = sub_activity[2].textContent;
            let c_activity_id = sub_activity[3].id;

            let c_id = $(this).attr('data-sub_activity_id');

            $('#editSubActivityName').val(c_name);
            $('#editSubActivityDescription').val(c_description);
            $('#editMainActivity').val(c_activity_id);
            $('#editSubActivityModal form').attr('action' , `sub-activity/${c_id}`);

            $('#editSubActivityModal').modal('show');
        });


        $('#delete_sub_activities_btn').click(function(){
            $('#delete_sub_activities_form').empty();

            var csrfVar = $('meta[name="csrf-token"]').attr('content');

            $("#delete_sub_activities_form").append("<input name='_token' value='" + csrfVar + "' type='hidden'>");

            $('table tbody input[type="checkbox"]').each(function(){
                 if($(this).is(":checked"))
                 {
                    $('#delete_sub_activities_form').append(`<input type="hidden" value="${$(this).val()}"  name="del_${$(this).val()}"/>`);
                 }
           });

          $('#delete_sub_activities_form').submit();
        });


        $('.delete').click(function(){
            let form = $(this).parent().children().last();
            form.submit();
        });
    });
</script>
@endsection



