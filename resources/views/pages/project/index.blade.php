@extends('pages.index')

@section('page_title')
Projects
@endsection

@section('styles')
<style>

    a{
        cursor: pointer;
    }
    .image-item{
        position: relative;
        max-width: 150px;
        width: 150px;
        height: 80px;
        margin: 8px;
        border-radius: 10px;
        overflow: hidden;
        cursor: pointer;
    }

    .image-item  img{
        width: 100%;
        max-height: 80px;
        display: block;
    }

    .overlay{
        width: 100%;
        height:100%;
        position: absolute;
        top:0;
        opacity:0;
        background-color: #fff;
        display: flex;
        justify-content: center;
        align-items: center;
    }




    .image-item:hover  .overlay{
        opacity: 0.5;
    }

    .image-box{
        display: flex;
        justify-content: flex-start;
        flex-wrap: wrap;
        width:100%;
    }

   .delete-image-btn{
       background-color: transparent;
       border: none;
       color: red;
   }

   .preview-image-btn > i{
       font-size: 25px !important;
       color: blue !important;
    }
</style>

<script>
    $(document).ready(function(){
        // Activate tooltip
        $('[data-toggle="tooltip"]').tooltip();

        // Select/Deselect checkboxes
        var checkbox = $('table tbody input[type="checkbox"]');
        $("#selectAll").click(function(){
            if(this.checked){
                checkbox.each(function(){
                    this.checked = true;
                });
            } else{
                checkbox.each(function(){
                    this.checked = false;
                });
            }
        });
        checkbox.click(function(){
            if(!this.checked){
                $("#selectAll").prop("checked", false);
            }
        });
    });
    </script>


@endsection

@section('content')
<div class="container-fluid">
	<div class="table-responsive">
		<div class="table-wrapper">
			<div class="table-title">

                <div class="row">

                    <div class="col-sm-6">
						<h2>Manage <b>Projects</b></h2>
					</div>

                    <div class="col-sm-6">
						<button id="add_modal_btn"     class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Add New Project</span></button>
						<button id="delete_projects_btn" class="btn btn-danger" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Delete</span></button>
					</div>

                </div>
			</div>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>
							<span class="custom-checkbox">
								<input type="checkbox" id="selectAll">
								<label for="selectAll"></label>
							</span>
						</th>
						<th>Name</th>
						<th>Description</th>
                        <th>images</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
				  @if(count($projects) > 0)
                      @foreach ($projects as $project)
                        <tr>
                            <td width="10%">
                                <span class="custom-checkbox">
                                    <input type="checkbox" id="checkbox{{$project->id}}" name="options[]" value="{{$project->id}}">
                                    <label for="checkbox{{$project->id}}"></label>
                                </span>
                            </td>
                            <td width="20%">{{$project->project_name}}</td>
                            <td width="20%">{{$project->description}}</td>
                            <td width="40%" >
                                <div class="image-box">
                                    @if(count($project->images) > 0)
                                        @foreach ( $project->images as $image)
                                            <div class="image-item" style="position: relative">
                                                <img src="{{$image->path}}" alt="error im image" width="100%" height="100%">
                                                <div class="overlay">
                                                    <a  class="preview-image-btn" href="{{$image->path}}" target="_blank">
                                                        <i class="material-icons" data-toggle="tooltip" title="Preview image">&#xe417;</i>
                                                    </a>

                                                    <form  method="post" action="{{route('image.destroy' , [$image->id])}}" >
                                                        @csrf
                                                        @method('delete')
                                                        <button type="submit" class="material-icons delete-image-btn" data-toggle="tooltip" title="Delete image">&#xE872;</button>
                                                    </form>

                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                      <div>
                                          No images avilable for this project
                                      </div>
                                    @endif
                                </div>
                            </td>
                            <td width="10%">
                                <a class="edit"   data-project_id="{{$project->id}}" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                <a class="delete" data-toggle="modal">
                                <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                                <form method="post" action="project/{{$project->id}}">
                                    @method('delete')
                                    @csrf
                                </form>
                            </td>
                        </tr>
                      @endforeach
                  @else
                     <tr style="text-align: center">
                         <td colspan="5">No Data Avilable</td>
                     </tr>
                  @endif

				</tbody>
			</table>
        <!--
			<div class="clearfix">
				<div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>
				<ul class="pagination">
					<li class="page-item disabled"><a href="#">Previous</a></li>
					<li class="page-item"><a href="#" class="page-link">1</a></li>
					<li class="page-item"><a href="#" class="page-link">2</a></li>
					<li class="page-item active"><a href="#" class="page-link">3</a></li>
					<li class="page-item"><a href="#" class="page-link">4</a></li>
					<li class="page-item"><a href="#" class="page-link">5</a></li>
					<li class="page-item"><a href="#" class="page-link">Next</a></li>
				</ul>
			</div>
        -->
		</div>
	</div>
</div>
<!-- Add Modal HTML -->
@include('modals.project.addProject')
<!-- Edit Modal HTML -->
@include('modals.project.editProject')
<!-- Delete Modal HTML -->
@include('modals.project.deleteProject')

<form action="delete_projects"  method="post" id="delete_projects_form">

</form>
@endsection

@section('scripts')

<script>
    $(document).ready(function(){

        $('#add_modal_btn').click(function(){
            $('#addProjectModal').modal('show');
        });

        $('.edit').click(function(){
            let project = $(this).parent().parent().children();

            let b_name = project[1].textContent;
            let b_description = project[2].textContent;
            let b_id = $(this).attr('data-project_id');

            $('#editProjectName').val(b_name);
            $('#editProjectDescription').val(b_description);

            $('#editProjectModal form').attr('action' , `project/${b_id}`);
            $('#editProjectModal').modal('show');

        });


        $('#delete_projects_btn').click(function(){
            $('#delete_projects_form').empty();

            var csrfVar = $('meta[name="csrf-token"]').attr('content');

            $("#delete_projects_form").append("<input name='_token' value='" + csrfVar + "' type='hidden'>");

            $('table tbody input[type="checkbox"]').each(function(){
                 if($(this).is(":checked"))
                 {
                    $('#delete_projects_form').append(`<input type="hidden" value="${$(this).val()}"  name="del_${$(this).val()}"/>`);
                 }
           });

          $('#delete_projects_form').submit();
        });


        $('.delete').click(function(){
            let form = $(this).parent().children().last();
            form.submit();
        });
    });
</script>
@endsection



