@extends('pages.index')

@section('page_title')
@endsection


@section('styles')
@endsection


@section('content')
<section id="ticket_section" class="contact">
    <div class="container" data-aos="fade-up">
      <div class="section-title">
        <h2>Ticket Setting</h2>
      </div>
        <div class="row">
            <div class="col-lg-12 mt-5 mt-lg-0 d-flex align-items-stretch">
            <form action="{{route('update_times')}}" method="get" role="form" class="form">

                <div class="form-group">
                    <label for="ctitical">Critical Time</label>
                    <input type="number" class="form-control"  id="ctitical" name="critical_time" min="1" value="{{$critical_time}}">
                </div>

                <div class="form-group">
                    <label for="warning">Warning Time</label>
                    <input type="number" class="form-control"  id="warning" name="warning_time" min="1" value="{{$warning_time}}">
                </div>

                <div class="text-center">
                    <button type="submit">Submit</button>
                </div>
            </form>
            </div>

        </div>
    </div>
</section>
@endsection


@section('scripts')
<script>
    $(document).ready(function(){
        window.location.href = "#ticket_section";
    })
</script>


@endsection
