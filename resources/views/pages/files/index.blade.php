@extends('pages.index')

@section('page_title')
Files
@endsection

@section('styles')
<style>

    a{
        cursor: pointer;
    }
    .image-item{
        position: relative;
        max-width: 150px;
        width: 150px;
        height: 80px;
        margin: 8px;
        border-radius: 10px;
        overflow: hidden;
        cursor: pointer;
    }

    .image-item  img{
        width: 100%;
        max-height: 80px;
        display: block;
    }

    .overlay{
        width: 100%;
        height:100%;
        position: absolute;
        top:0;
        opacity:0;
        background-color: #fff;
        display: flex;
        justify-content: center;
        align-items: center;
    }




    .image-item:hover  .overlay{
        opacity: 0.5;
    }

    .image-box{
        display: flex;
        justify-content: flex-start;
        flex-wrap: wrap;
        width:100%;
    }

   .delete-image-btn{
       background-color: transparent;
       border: none;
       color: red;
   }

   .preview-image-btn > i{
       font-size: 25px !important;
       color: blue !important;
    }
</style>

<script>
    $(document).ready(function(){
        // Activate tooltip
        $('[data-toggle="tooltip"]').tooltip();

        // Select/Deselect checkboxes
        var checkbox = $('table tbody input[type="checkbox"]');
        $("#selectAll").click(function(){
            if(this.checked){
                checkbox.each(function(){
                    this.checked = true;
                });
            } else{
                checkbox.each(function(){
                    this.checked = false;
                });
            }
        });
        checkbox.click(function(){
            if(!this.checked){
                $("#selectAll").prop("checked", false);
            }
        });
    });
    </script>


@endsection

@section('content')
<div class="container-fluid">
	<div class="table-responsive">
		<div class="table-wrapper">
			<div class="table-title">

                <div class="row">

                    <div class="col-sm-6">
						<h2>Manage <b>Files</b></h2>
					</div>

                    <div class="col-sm-6">
						<button id="add_modal_btn"     class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Add New File</span></button>
						<button id="delete_files_btn" class="btn btn-danger" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Delete</span></button>
					</div>

                </div>
			</div>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>
							<span class="custom-checkbox">
								<input type="checkbox" id="selectAll">
								<label for="selectAll"></label>
							</span>
						</th>
						<th>File Name</th>
                        <th>Type</th>
						<th>Description</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
				  @if(count($files) > 0)
                      @foreach ($files as $file)
                        <tr>
                            <td width="10%">
                                <span class="custom-checkbox">
                                    <input type="checkbox" id="checkbox{{$file->id}}" name="options[]" value="{{$file->id}}">
                                    <label for="checkbox{{$file->id}}"></label>
                                </span>
                            </td>
                            <td width="25%">{{$file->file_name}}</td>
                            <td width="25%">{{$file->type}}</td>
                            <td width="30%">{{$file->description}}</td>
                            <td width="20%">
                                <a class="preview"   data-file_id="{{$file->id}}" data-toggle="modal" href="{{$file->file_path}}" target="_blank">
                                    <i class="material-icons" data-toggle="tooltip" title="Preview">&#xe417;</i>
                                </a>

                                <a class="edit"   data-file_id="{{$file->id}}" data-toggle="modal">
                                    <i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i>
                                </a>

                                <a class="delete" data-toggle="modal">
                                <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                                <form method="post" action="file/{{$file->id}}">
                                    @method('delete')
                                    @csrf
                                </form>
                            </td>
                        </tr>
                      @endforeach
                  @else
                     <tr style="text-align: center">
                         <td colspan="5">No Data Avilable</td>
                     </tr>
                  @endif

				</tbody>
			</table>
        <!--
			<div class="clearfix">
				<div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>
				<ul class="pagination">
					<li class="page-item disabled"><a href="#">Previous</a></li>
					<li class="page-item"><a href="#" class="page-link">1</a></li>
					<li class="page-item"><a href="#" class="page-link">2</a></li>
					<li class="page-item active"><a href="#" class="page-link">3</a></li>
					<li class="page-item"><a href="#" class="page-link">4</a></li>
					<li class="page-item"><a href="#" class="page-link">5</a></li>
					<li class="page-item"><a href="#" class="page-link">Next</a></li>
				</ul>
			</div>
        -->
		</div>
	</div>
</div>
<!-- Add Modal HTML -->
@include('modals.file.addFile')
<!-- Edit Modal HTML -->
@include('modals.file.editFile')
<!-- Delete Modal HTML -->
@include('modals.file.deleteFile')

<form action="delete_files"  method="post" id="delete_files_form">

</form>
@endsection

@section('scripts')

<script>
    $(document).ready(function(){

        $('#add_modal_btn').click(function(){
            $('#addFileModal').modal('show');
        });

        $('.edit').click(function(){
            let file = $(this).parent().parent().children();
           console.log(file);
            let b_name = file[1].textContent;
            let b_type = file[2].textContent;
            let b_description = file[3].textContent;
            let b_id = $(this).attr('data-file_id');

            $('#editFileName').val(b_name);
            $('#editfFileType').val(b_type);
            $('#editfileDescription').val(b_description);

            $('#editFileModal form').attr('action' , `file/${b_id}`);
            $('#editFileModal').modal('show');

        });


        $('#delete_files_btn').click(function(){
            $('#delete_files_form').empty();

            var csrfVar = $('meta[name="csrf-token"]').attr('content');

            $("#delete_files_form").append("<input name='_token' value='" + csrfVar + "' type='hidden'>");

            $('table tbody input[type="checkbox"]').each(function(){
                 if($(this).is(":checked"))
                 {
                    $('#delete_files_form').append(`<input type="hidden" value="${$(this).val()}"  name="del_${$(this).val()}"/>`);
                 }
           });

          $('#delete_files_form').submit();
        });


        $('.delete').click(function(){
            let form = $(this).parent().children().last();
            form.submit();
        });
    });
</script>
@endsection



