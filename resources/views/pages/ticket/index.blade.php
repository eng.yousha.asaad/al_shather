@extends('pages.index')

@section('page_title')
Tickets
@endsection

@section('styles')
<style>
    .btn-download{
        color: #28a745 !important;
    }
    .btn-download:hover{
        color: white !important;
    }

</style>
<script>
    $(document).ready(function(){
        // Activate tooltip
        $('[data-toggle="tooltip"]').tooltip();

        // Select/Deselect checkboxes
        var checkbox = $('table tbody input[type="checkbox"]');
        $("#selectAll").click(function(){
            if(this.checked){
                checkbox.each(function(){
                    this.checked = true;
                });
            } else{
                checkbox.each(function(){
                    this.checked = false;
                });
            }
        });
        checkbox.click(function(){
            if(!this.checked){
                $("#selectAll").prop("checked", false);
            }
        });
    });
    </script>


@endsection
@section('content')
<div class="container-fluid">
	<div class="table-responsive">
		<div class="table-wrapper">
			<div class="table-title">

                <div class="row">
                    <div class="col-sm-6">
						<h2>Manage <b>Tickets</b></h2>
					</div>

                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-8">
                                <form method="get" action="{{route('ticket.index')}}" id="ticket_type_form">
                                    <select class="custom-select" id="ticket_type" name="ticket_type"
                                     style="width: 50% !important">
                                       <option value="all" hidden selected>Select Ticket type</option>
                                        <option  @if(request()->input('ticket_type') == 'all') selected @endif value="all">all</option>
                                        <option  @if(request()->input('ticket_type') == 'new') selected @endif value="new">New</option>
                                        <option  @if(request()->input('ticket_type') == 'solved') selected @endif value="solved">Solved</option>
                                        <option  @if(request()->input('ticket_type') == 'critical') selected @endif value="critical">critical</option>
                                        <option  @if(request()->input('ticket_type') == 'warning') selected @endif value="warning" >warning</option>
                                      </select>
                                </form>
                            </div>
                            <div class="col-sm-4">
                                <button id="resolve_all_btn" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xe834;</i> <span>Resolve all</span></button>
                            </div>
                        </div>

					</div>

                </div>
			</div>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>
							<span class="custom-checkbox">
								<input type="checkbox" id="selectAll">
								<label for="selectAll"></label>
							</span>
						</th>
						<th>Subject</th>
						<th>Description</th>
                        <th>File</th>
                        <th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
				  @if(count($tickets) > 0)
                      @foreach ($tickets as $ticket)
                        <tr>
                            <td width="5%">
                                <span class="custom-checkbox">
                                    <input type="checkbox" @if($ticket->solved) disabled @endif id="checkbox{{$ticket->id}}" name="options[]" value="{{$ticket->id}}">
                                    <label for="checkbox{{$ticket->id}}"></label>
                                </span>
                            </td>
                            <td width="20%">{{$ticket->subject}}</td>
                            <td width="40%">{{$ticket->description}}</td>
                            <td width="15%" >
                                @if($ticket->file_path != null)
                                    <a class="btn btn-outline-success btn-download" href="{{$ticket->file_path}}" download="">Download</a>
                                @else
                                   No file for this ticket
                                @endif
                            </td>
                            <td width="15%">
                                @if($ticket->solved)
                                   <span class="alert alert-success">Solved</span>
                                @elseif (now()->diffInDays($ticket->created_at) >= $warning)
                                    <div class="alert alert-danger">pass more than {{$warning}} days without solving</div>
                                @elseif (now()->diffInDays($ticket->created_at) >= $critical)
                                   <div class="alert alert-warning">pass more than {{$critical}} days without solving</div>
                                @else
                                   <span class="alert alert-warning">not Solved yet</span>
                                @endif
                            </td>
                            <td width="5%">
                                @if(!$ticket->solved)
                                <form action="{{route('ticket.update' , [$ticket->id])}}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <button class="btn btn-primary btn-round"  type="submit">
                                        solve
                                    </button>
                                </form>

                                @endif
                            </td>
                        </tr>
                      @endforeach
                  @else
                     <tr style="text-align: center">
                         <td colspan="6">No Data Avilable</td>
                     </tr>
                  @endif

				</tbody>
			</table>

		</div>
	</div>
</div>

<form action="resolve_tickets"  method="post" id="resolve_tickets_form">

</form>

@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $('#ticket_type').change(function(){
            $('#ticket_type_form').submit();
        });

        $('#resolve_all_btn').click(function(){
            $('#resolve_tickets_form').empty();

            var csrfVar = $('meta[name="csrf-token"]').attr('content');

            $("#resolve_tickets_form").append("<input name='_token' value='" + csrfVar + "' type='hidden'>");

            $('table tbody input[type="checkbox"]').each(function(){
                 if($(this).is(":checked"))
                 {
                    $('#resolve_tickets_form').append(`<input type="hidden" value="${$(this).val()}"  name="resolve_${$(this).val()}"/>`);
                 }
           });

          $('#resolve_tickets_form').submit();
        });
    });
</script>

@endsection



