@extends('pages.index')

@section('page_title')
Users
@endsection

@section('styles')

<script>
    $(document).ready(function(){
        // Activate tooltip
        $('[data-toggle="tooltip"]').tooltip();

        // Select/Deselect checkboxes
        var checkbox = $('table tbody input[type="checkbox"]');
        $("#selectAll").click(function(){
            if(this.checked){
                checkbox.each(function(){
                    this.checked = true;
                });
            } else{
                checkbox.each(function(){
                    this.checked = false;
                });
            }
        });
        checkbox.click(function(){
            if(!this.checked){
                $("#selectAll").prop("checked", false);
            }
        });
    });
    </script>


@endsection
@section('content')
<div class="container-fluid">
	<div class="table-responsive">
		<div class="table-wrapper">
			<div class="table-title">

                <div class="row">
                    <div class="col-sm-6">
						<h2><b>Users Requests</b></h2>
					</div>
                    <div class="col-sm-6">
						<button id="add_modal_btn"     data-bs-target=".addUserModal" class="btn btn-success" data-bs-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Add New User</span></button>
						<button id="verify_users_btn"   class="btn btn-info" data-toggle="modal"><i class="material-icons">check</i> <span>Verify All Checked</span></button>
					</div>
                </div>
			</div>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>
							<span class="custom-checkbox">
								<input type="checkbox" id="selectAll">
								<label for="selectAll"></label>
							</span>
						</th>
						<th>Name</th>
						<th>Email</th>
                        <th>Company</th>
                        <th>Mobile</th>
                        <th>Request Date</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
				  @if(count($users_requests) > 0)
                      @foreach ($users_requests as $user)
                        <tr>
                            <td width="5%">
                                <span class="custom-checkbox">
                                    <input type="checkbox"  id="checkbox{{$user->id}}" name="options[]" value="{{$user->id}}">
                                    <label for="checkbox{{$user->id}}"></label>
                                </span>
                            </td>
                            <td width="18%">{{$user->name}}</td>
                            <td width="18%">{{$user->email}}</td>
                            <td width="18%" >{{$user->company}}</td>
                            <td width="18%" >{{Crypt::decrypt($user->phone);}}</td>
                            <td width="18%">{{$user->created_at}}</td>
                            <td width="5%">
                                <form action="{{route('user.update' , [$user->id])}}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <button class="btn btn-primary btn-round"  type="submit">
                                        Verify
                                    </button>
                                </form>
                            </td>
                        </tr>
                      @endforeach
                  @else
                     <tr style="text-align: center">
                         <td colspan="7">No Data Avilable</td>
                     </tr>
                  @endif

				</tbody>
			</table>

		</div>
	</div>
</div>

<form action="verify_all_users"  method="post" id="verify_all_users_form">

</form>


@include('modals.user.addUser')

@endsection

@section('scripts')
<script>
    $(document).ready(function(){

        $('#verify_users_btn').click(function(){
            $('#verify_all_users_form').empty();

            var csrfVar = $('meta[name="csrf-token"]').attr('content');

            $("#verify_all_users_form").append("<input name='_token' value='" + csrfVar + "' type='hidden'>");

            $('table tbody input[type="checkbox"]').each(function(){
                 if($(this).is(":checked"))
                 {
                    $('#verify_all_users_form').append(`<input type="hidden" value="${$(this).val()}"  name="resolve_${$(this).val()}"/>`);
                 }
           });

          $('#verify_all_users_form').submit();
        });
    });
</script>

@endsection



