@extends('pages.index')

@section('page_title')
Clients
@endsection

@section('styles')
<style>

    a{
        cursor: pointer;
    }
    .image-item{
        position: relative;
        max-width: 150px;
        width: 150px;
        height: 80px;
        margin: 8px;
        border-radius: 10px;
        overflow: hidden;
        cursor: pointer;
    }

    .image-item  img{
        width: 100%;
        max-height: 80px;
        display: block;
    }

    .overlay{
        width: 100%;
        height:100%;
        position: absolute;
        top:0;
        opacity:0;
        background-color: #fff;
        display: flex;
        justify-content: center;
        align-items: center;
    }




    .image-item:hover  .overlay{
        opacity: 0.5;
    }

    .image-box{
        display: flex;
        justify-content: flex-start;
        flex-wrap: wrap;
        width:100%;
    }

   .delete-image-btn{
       background-color: transparent;
       border: none;
       color: red;
   }

   .preview-image-btn > i{
       font-size: 25px !important;
       color: blue !important;
    }
</style>

<script>
    $(document).ready(function(){
        // Activate tooltip
        $('[data-toggle="tooltip"]').tooltip();

        // Select/Deselect checkboxes
        var checkbox = $('table tbody input[type="checkbox"]');
        $("#selectAll").click(function(){
            if(this.checked){
                checkbox.each(function(){
                    this.checked = true;
                });
            } else{
                checkbox.each(function(){
                    this.checked = false;
                });
            }
        });
        checkbox.click(function(){
            if(!this.checked){
                $("#selectAll").prop("checked", false);
            }
        });
    });
    </script>


@endsection

@section('content')
<div class="container-fluid">
	<div class="table-responsive">
		<div class="table-wrapper">
			<div class="table-title">

                <div class="row">

                    <div class="col-sm-6">
						<h2>Manage <b>Clients</b></h2>
					</div>

                    <div class="col-sm-6">
						<button id="add_modal_btn"     class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Add New Client</span></button>
						<button id="delete_clients_btn" class="btn btn-danger" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Delete</span></button>
					</div>

                </div>
			</div>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>
							<span class="custom-checkbox">
								<input type="checkbox" id="selectAll">
								<label for="selectAll"></label>
							</span>
						</th>
						<th>Client Name</th>
                        <th>Client Logo</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
				  @if(count($clients) > 0)
                      @foreach ($clients as $client)
                        <tr>
                            <td width="10%">
                                <span class="custom-checkbox">
                                    <input type="checkbox" id="checkbox{{$client->id}}" name="options[]" value="{{$client->id}}">
                                    <label for="checkbox{{$client->id}}"></label>
                                </span>
                            </td>
                            <td width="40%">{{$client->client_name}}</td>
                            <td width="40%" >
                                <div class="image-box">
                                    @if(count($client->images) > 0)
                                        @foreach ( $client->images as $image)
                                            <div class="image-item" style="position: relative">
                                                <img src="{{$image->path}}" alt="error im image" width="100%" height="100%">
                                                <div class="overlay">
                                                    <a  class="preview-image-btn" href="{{$image->path}}" target="_blank">
                                                        <i class="material-icons" data-toggle="tooltip" title="Preview image">&#xe417;</i>
                                                    </a>

                                                    <!--
                                                    <form  method="post" action="{{route('image.destroy' , [$image->id])}}" >
                                                        @csrf
                                                        @method('delete')
                                                        <button type="submit" class="material-icons delete-image-btn" data-toggle="tooltip" title="Delete image">&#xE872;</button>
                                                    </form>
                                                -->
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                      <div>
                                          No images avilable for this Client
                                      </div>
                                    @endif
                                </div>
                            </td>
                            <td width="10%">
                                <a class="edit"   data-client_id="{{$client->id}}" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                <a class="delete" data-toggle="modal">
                                <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                                <form method="post" action="client/{{$client->id}}">
                                    @method('delete')
                                    @csrf
                                </form>
                            </td>
                        </tr>
                      @endforeach
                  @else
                     <tr style="text-align: center">
                         <td colspan="4">No Data Avilable</td>
                     </tr>
                  @endif

				</tbody>
			</table>

		</div>
	</div>
</div>
<!-- Add Modal HTML -->
@include('modals.client.addClient')
<!-- Edit Modal HTML -->
@include('modals.client.editClient')
<!-- Delete Modal HTML -->
@include('modals.client.deleteClient')

<form action="delete_clients"  method="post" id="delete_clients_form">

</form>
@endsection

@section('scripts')

<script>
    $(document).ready(function(){

        $('#add_modal_btn').click(function(){
            $('#addClientModal').modal('show');
        });

        $('.edit').click(function(){
            let client = $(this).parent().parent().children();

            console.log(client);
            let b_name = client[1].textContent;
            let b_id = $(this).attr('data-client_id');

            $('#editClientName').val(b_name);

            $('#editClientModal form').attr('action' , `client/${b_id}`);
            $('#editClientModal').modal('show');
        });


        $('#delete_clients_btn').click(function(){
            $('#delete_clients_form').empty();

            var csrfVar = $('meta[name="csrf-token"]').attr('content');

            $("#delete_clients_form").append("<input name='_token' value='" + csrfVar + "' type='hidden'>");

            $('table tbody input[type="checkbox"]').each(function(){
                 if($(this).is(":checked"))
                 {
                    $('#delete_clients_form').append(`<input type="hidden" value="${$(this).val()}"  name="del_${$(this).val()}"/>`);
                 }
           });

          $('#delete_clients_form').submit();
        });


        $('.delete').click(function(){
            let form = $(this).parent().children().last();
            form.submit();
        });
    });
</script>
@endsection



