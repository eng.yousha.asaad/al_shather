@extends('pages.index')

@section('page_title')
{{translate('Downloads')}}
@endsection

@section('styles')
<style>
  tr > td{
    word-wrap: break-word;
  }

  .center-text{
    text-align: center;
  }
</style>

@endsection

@section('content')
<section id="new_ticket" class="contact">
    <div class="container" data-aos="fade-up">
      <div class="section-title">
        <h2>{{translate('Downloads')}}</h2>
        <div class="row">
            <div class="col-md-2">
                <label for="filterFileType" class="pt-2">{{translate('Filter by File Type')}}</label>
            </div>
            <div class="col-md-4">
                <select  id="filterFileType" class="custom-select" >
                    <option value="" hidden>{{translate('Select File Type')}}</option>
                    <option value="all">{{translate('All Types')}}</option>
                    <option value="Datasheet">{{translate('Datasheet')}}</option>
                    <option value="Training File">{{translate('Training File')}}</option>
                    <option value="QIG&User Manual">{{translate('User Manual')}}</option>
                    <option value="Firmware">{{translate('Firmware')}}</option>
                    <option value="Tool">{{translate('Tool')}}</option>
                </select>
            </div>


        </div>
      </div>
      <div class="row">
        <table class="table table-striped">
            <thead>
              <tr>
                <th width="30%" >{{translate('File Name')}}</th>
                <th width="30%">{{translate('File Type')}}</th>
                <th width="30%">{{translate('Description')}}</th>
                <th width="10%"></th>
              </tr>
            </thead>
            <tbody>
             @if(count($downloads) > 0)
                @foreach ($downloads as $download)
                <tr class="break-words">
                    <td width="30%">{{translate($download->file_name)}}</td>
                    <td width="30%">{{translate($download->type)}}</td>
                    <td width="30%">{{translate($download->description)}}</td>
                    <td width="10%">
                        <a href="{{$download->file_path}}" download="" class="btn btn-primary-outline">{{translate('Download')}}</a>
                    </td>
                </tr>
                @endforeach
             @else
               <tr class="center-text">
                   <td colspan="4" wi>No Downloads Available</td>
               </tr>
             @endif
            </tbody>
          </table>
      </div>
    </div>
</section>
@endsection

@section('scripts')

<script>
    $(document).ready(function(){

        //filter result
        $('#filterFileType').change(function(){
            window.location.href = `/downloads/${$(this).val()}`;
        });
    });
</script>
@endsection
