@extends('pages.index')

@section('page_title')
 {{translate('Create New Ticket')}}
@endsection


@section('styles')
@endsection


@section('content')
<section id="new_ticket" class="contact" >
    <div class="container" data-aos="fade-up">
      <div class="section-title">
        <h2>{{translate('Create New Ticket')}}</h2>
        <div class="alert alert-info">
            {{translate('After Create Ticket Successfully. Please wait a call or email from our support department')}}
        </div>
      </div>
        <div class="row">
            <div class="col-lg-12 mt-5 mt-lg-0 d-flex align-items-stretch @if(Session::get('lang') == 'en') t-left @else t-right @endif"  @if(Session::get('lang') == 'en') dir="ltr" @else dir="rtl" @endif>
            <form action="{{route('ticket.store')}}" method="post" role="form" class="form" enctype="multipart/form-data">
                @csrf
                <div class="form-group" dir>
                    <label for="email">{{translate('Email Address')}}</label>
                    <input type="email" class="form-control"  id="email" name="email" disabled value="{{auth()->user()->email}}">
                </div>
                <div class="form-group">
                <label for="subject">{{translate('Subject')}} *</label>
                <input type="text" class="form-control" name="subject" id="subject" required>
                </div>

                <div class="form-group">
                <label for="description">{{translate('Description')}} *</label>
                <textarea class="form-control" name="description" rows="10" id="description" required></textarea>
                </div>

                <div class="form-group">
                    <label for="file">{{translate('Upload file=(optional)')}}</label>
                    <input type="file" class="form-control" name="file" id="file">
                    </div>

                <div class="text-center">
                    <button type="submit">{{translate('Submit')}}</button>
                </div>
            </form>
            </div>

        </div>
    </div>
</section>
@endsection


@section('scripts')
<script>
    $(document).ready(function(){
        window.location.href = "#new_ticket";
    })
</script>


@endsection
