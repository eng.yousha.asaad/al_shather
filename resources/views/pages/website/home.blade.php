@extends('pages.index')

@section('page_title')
Home Page
@endsection

@section('styles')
<link rel="stylesheet" href=
"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>

    *{
    padding: 0;
    margin: 0;
    -webkit-box-sizing: border-box;
            box-sizing: border-box;
}

html{
    overflow-y: scroll;
}

body{
    background-color: transparent;
    background: url('https://i.imgur.com/ApQKtQZ.jpeg') fixed no-repeat;
    background-size: cover;
    max-height: 300rem;
    border: 15px solid transparent !important;
    -o-border-image: linear-gradient(to bottom right,rgba(255, 255, 255, 0.75) 0%,rgba(245, 245, 245, 0.75) 0%,rgba(245, 245, 245, 0.75) 16.6%,rgb(245, 245, 245) 37.8%,rgb(245, 245, 245) 48.8%,rgb(254, 254, 254) 53.1%,rgba(245, 245, 245, 0.75) 79.4%,rgba(245, 245, 245, 0.75) 84.3%) !important;
       border-image: -webkit-gradient(linear,left top, right bottom,from(rgba(255, 255, 255, 0.75)),color-stop(0%, rgba(245, 245, 245, 0.75)),color-stop(16.6%, rgba(245, 245, 245, 0.75)),color-stop(37.8%, rgb(245, 245, 245)),color-stop(48.8%, rgb(245, 245, 245)),color-stop(53.1%, rgb(254, 254, 254)),color-stop(79.4%, rgba(245, 245, 245, 0.75)),color-stop(84.3%, rgba(245, 245, 245, 0.75))) !important;
       border-image: linear-gradient(to bottom right,rgba(255, 255, 255, 0.75) 0%,rgba(245, 245, 245, 0.75) 0%,rgba(245, 245, 245, 0.75) 16.6%,rgb(245, 245, 245) 37.8%,rgb(245, 245, 245) 48.8%,rgb(254, 254, 254) 53.1%,rgba(245, 245, 245, 0.75) 79.4%,rgba(245, 245, 245, 0.75) 84.3%) !important;
    border-image-slice: 1 !important;
}

a{
    text-decoration: none;
    color: inherit;
    cursor: pointer;
    opacity: 0.95;
}
.country_contact{
        text-align: center;
    }

a:hover{
  opacity: 1;
  color: black;
}
/*----------------
Push Bar
------------------*/
.bar-cont{
    position: relative;
    top: -215px;
}

.push-bar{
    position: relative;
    margin: auto;
    width: 500px;
    height: 200px;
    border-bottom-left-radius: 5px;
    border-bottom-right-radius: 5px;
}

.creator{
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: horizontal;
  -webkit-box-direction: normal;
      -ms-flex-direction: row;
          flex-direction: row;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
}

.creator #codepen{
  text-align: center;
  font-size: 180px;
  margin-top: 30px;
  border: 2px solid white;
  border-radius: 50%;
  -webkit-box-shadow: 0px 8px 5px grey;
          box-shadow: 0px 8px 5px grey;
}


.arrow{
	margin: auto;
    padding: 0;
    margin-top: 50px;
    margin-bottom: 0;
    max-width: 80px;
    height: 80px;
    border: 3px solid black;
    border-radius: 50%;
    background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxNi4wLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+DQo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1MTIgNTEyIiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxwYXRoIGZpbGw9IiNGRkZGRkYiIGQ9Ik0yOTMuNzUxLDQ1NS44NjhjLTIwLjE4MSwyMC4xNzktNTMuMTY1LDE5LjkxMy03My42NzMtMC41OTVsMCwwYy0yMC41MDgtMjAuNTA4LTIwLjc3My01My40OTMtMC41OTQtNzMuNjcyICBsMTg5Ljk5OS0xOTBjMjAuMTc4LTIwLjE3OCw1My4xNjQtMTkuOTEzLDczLjY3MiwwLjU5NWwwLDBjMjAuNTA4LDIwLjUwOSwyMC43NzIsNTMuNDkyLDAuNTk1LDczLjY3MUwyOTMuNzUxLDQ1NS44Njh6Ii8+DQo8cGF0aCBmaWxsPSIjRkZGRkZGIiBkPSJNMjIwLjI0OSw0NTUuODY4YzIwLjE4LDIwLjE3OSw1My4xNjQsMTkuOTEzLDczLjY3Mi0wLjU5NWwwLDBjMjAuNTA5LTIwLjUwOCwyMC43NzQtNTMuNDkzLDAuNTk2LTczLjY3MiAgbC0xOTAtMTkwYy0yMC4xNzgtMjAuMTc4LTUzLjE2NC0xOS45MTMtNzMuNjcxLDAuNTk1bDAsMGMtMjAuNTA4LDIwLjUwOS0yMC43NzIsNTMuNDkyLTAuNTk1LDczLjY3MUwyMjAuMjQ5LDQ1NS44Njh6Ii8+DQo8L3N2Zz4=);
    background-repeat: no-repeat;
    background-position: center;
    background-size: 45%;
    background-color: black;
    -webkit-transition: -webkit-transform 0.3s ease-in-out;
    transition: -webkit-transform 0.3s ease-in-out;
    transition: transform 0.3s ease-in-out;
    transition: transform 0.3s ease-in-out, -webkit-transform 0.3s ease-in-out;
    -webkit-animation: blockAppear .6s ease-in-out;
            animation: blockAppear .6s ease-in-out;
    -webkit-animation-duration: 0.5s;
            animation-duration: 0.5s;
}

@-webkit-keyframes bounce {
	0%, 20%, 50%, 80%, 100% {
    -webkit-transform: translateY(0);
            transform: translateY(0);
  }
	40% {
    -webkit-transform: translateY(-30px);
            transform: translateY(-30px);
  }
	60% {
    -webkit-transform: translateY(-15px);
            transform: translateY(-15px);
  }
}

@keyframes bounce {
	0%, 20%, 50%, 80%, 100% {
    -webkit-transform: translateY(0);
            transform: translateY(0);
  }
	40% {
    -webkit-transform: translateY(-30px);
            transform: translateY(-30px);
  }
	60% {
    -webkit-transform: translateY(-15px);
            transform: translateY(-15px);
  }
}
/*-----------------
Cards
------------------*/
.main-cont{
    padding: 0;
    margin: 0;
    top: -215px;
}

.news-row {
	margin: 0;
    margin-top: 50px;
    margin-bottom: 50px;
}

.news-block{
    margin: auto;
    padding: 0;
    background-color: transparent;
    max-width: 1060px;
    min-width: 100px;
    border: 20px solid transparent;
    -o-border-image: linear-gradient(to bottom right,rgba(255, 255, 255, 0.75) 0%,rgba(245, 245, 245, 0.75) 0%,rgba(245, 245, 245, 0.75) 16.6%,rgb(245, 245, 245) 37.8%,rgb(245, 245, 245) 48.8%,rgb(254, 254, 254) 53.1%,rgba(245, 245, 245, 0.75) 79.4%,rgba(245, 245, 245, 0.75) 84.3%) !important;
       border-image: -webkit-gradient(linear,left top, right bottom,from(rgba(255, 255, 255, 0.75)),color-stop(0%, rgba(245, 245, 245, 0.75)),color-stop(16.6%, rgba(245, 245, 245, 0.75)),color-stop(37.8%, rgb(245, 245, 245)),color-stop(48.8%, rgb(245, 245, 245)),color-stop(53.1%, rgb(254, 254, 254)),color-stop(79.4%, rgba(245, 245, 245, 0.75)),color-stop(84.3%, rgba(245, 245, 245, 0.75))) !important;
       border-image: linear-gradient(to bottom right,rgba(255, 255, 255, 0.75) 0%,rgba(245, 245, 245, 0.75) 0%,rgba(245, 245, 245, 0.75) 16.6%,rgb(245, 245, 245) 37.8%,rgb(245, 245, 245) 48.8%,rgb(254, 254, 254) 53.1%,rgba(245, 245, 245, 0.75) 79.4%,rgba(245, 245, 245, 0.75) 84.3%) !important;
    border-image-slice: 1 !important;
    -webkit-transition: all 0.6s ease;
    transition: all 0.6s ease;
    -webkit-animation: blockAppear .6s ease-in-out;
            animation: blockAppear .6s ease-in-out;
    -webkit-animation-duration: 1s;
            animation-duration: 1s;
}

.underlay{
  /*display: none;*/
  margin: 0;
  padding: 0;
  max-height: 350px;
  max-width: 340px;
}

.card{
    margin: 0;
    width: 100%;
    max-height: 350px;
    max-width: 100%;
    background-color: transparent;
    border: 20px solid transparent !important;
    -o-border-image: linear-gradient(to bottom right,rgba(255, 255, 255, 0.75) 0%,rgba(245, 245, 245, 0.75) 0%,rgba(245, 245, 245, 0.75) 16.6%,rgba(245, 245, 245, 0.75) 79.4%,rgba(245, 245, 245, 0.75) 84.3%) !important;
       border-image: -webkit-gradient(linear,left top, right bottom,from(rgba(255, 255, 255, 0.75)),color-stop(0%, rgba(245, 245, 245, 0.75)),color-stop(16.6%, rgba(245, 245, 245, 0.75)),color-stop(79.4%, rgba(245, 245, 245, 0.75)),color-stop(84.3%, rgba(245, 245, 245, 0.75))) !important;
       border-image: linear-gradient(to bottom right,rgba(255, 255, 255, 0.75) 0%,rgba(245, 245, 245, 0.75) 0%,rgba(245, 245, 245, 0.75) 16.6%,rgba(245, 245, 245, 0.75) 79.4%,rgba(245, 245, 245, 0.75) 84.3%) !important;
    border-image-slice: 1 !important;
    -webkit-transition: -webkit-transform 0.6s ease;
    transition: -webkit-transform 0.6s ease;
    transition: transform 0.6s ease;
    transition: transform 0.6s ease, -webkit-transform 0.6s ease;
    -webkit-animation: blockAppear .6s ease-in-out;
            animation: blockAppear .6s ease-in-out;
    -webkit-animation-duration: 1.5s;
            animation-duration: 1.5s;
}

@-webkit-keyframes blockAppear {
  0% {
    opacity: 0;
    -webkit-transform: translateY(20px) ;
            transform: translateY(20px) ;
  }
  ready.css:1
  40% {
    opacity: 0;
    -webkit-transform: translateY(20px);
            transform: translateY(20px);
    -webkit-box-shadow: 0 10px 35px rgba(0,0,0,.15), 0 1px 0 rgba(0,0,0,.15);
            box-shadow: 0 10px 35px rgba(0,0,0,.15), 0 1px 0 rgba(0,0,0,.15);
  }
  ready.css:1
  80% {
    opacity: 1;
    -webkit-transform: translateY(-5px);
            transform: translateY(-5px);
  }
  ready.css:1
  100% {
    opacity: 1;
    -webkit-transform: translateY(0);
            transform: translateY(0);
    -webkit-box-shadow: none;
            box-shadow: none;
  }
}

@keyframes blockAppear {
  0% {
    opacity: 0;
    -webkit-transform: translateY(20px) ;
            transform: translateY(20px) ;
  }
  ready.css:1
  40% {
    opacity: 0;
    -webkit-transform: translateY(20px);
            transform: translateY(20px);
    -webkit-box-shadow: 0 10px 35px rgba(0,0,0,.15), 0 1px 0 rgba(0,0,0,.15);
            box-shadow: 0 10px 35px rgba(0,0,0,.15), 0 1px 0 rgba(0,0,0,.15);
  }
  ready.css:1
  80% {
    opacity: 1;
    -webkit-transform: translateY(-5px);
            transform: translateY(-5px);
  }
  ready.css:1
  100% {
    opacity: 1;
    -webkit-transform: translateY(0);
            transform: translateY(0);
    -webkit-box-shadow: none;
            box-shadow: none;
  }
}

.card:hover{
    z-index: 999;
    max-height: 800px;
    width: 400px;
    max-width: 400px;
    border: none !important;
    border-right: 60px solid transparent !important;
    border-bottom: 20px solid transparent !important;
    margin-right: -20px;
	  -webkit-transform:  translate(-10px, -60px);
	          transform:  translate(-10px, -60px);
}

.card:before{
  -webkit-box-shadow: none;
          box-shadow: none;
  display: block;
  content: '';
  position: absolute;
  width: 100%;
  max-width: 400px;
  height: 100%;
  -webkit-transition: -webkit-box-shadow 0.6s ease;
  transition: -webkit-box-shadow 0.6s ease;
  transition: box-shadow 0.6s ease;
  transition: box-shadow 0.6s ease, -webkit-box-shadow 0.6s ease;
}

.card:hover:before
{
  max-width: 300px;
  -webkit-box-shadow: 15px 15px 5px RGBA(142, 142, 142, .6);
          box-shadow: 15px 15px 5px RGBA(142, 142, 142, .6);
}

.card:hover .card-img-top{
	height: 100px;
}

.card:hover .card-block {
  width: 300px;
	background-image: -webkit-gradient(linear,right bottom, left top,from(rgb(72, 85, 108)),color-stop(50%, rgb(27, 33, 43)),color-stop(51%, rgb(20, 25, 34)),to(rgb(53, 59, 69)));
	background-image: linear-gradient(to top left,rgb(72, 85, 108) 0%,rgb(27, 33, 43) 50%,rgb(20, 25, 34) 51%,rgb(53, 59, 69) 100%);
}

.card:hover .card-title{
    color: white;
}

.card-title{
    padding: 8px;
}

.card:hover .card-text{
	  /*display: block !important;*/
    color: white;
}

.card-block{
    background-color: transparent;
    background-image: -webkit-gradient(linear,left top, right bottom,from(rgba(255, 255, 255, 0.75)),color-stop(0%, rgba(245, 245, 245, 0.75)),color-stop(16.6%, rgba(245, 245, 245, 0.75)),color-stop(37.8%, rgb(245, 245, 245)),color-stop(48.8%, rgb(245, 245, 245)),color-stop(53.1%, rgb(254, 254, 254)),color-stop(79.4%, rgba(245, 245, 245, 0.75)),color-stop(84.3%, rgba(245, 245, 245, 0.75)));
    background-image: linear-gradient(to bottom right,rgba(255, 255, 255, 0.75) 0%,rgba(245, 245, 245, 0.75) 0%,rgba(245, 245, 245, 0.75) 16.6%,rgb(245, 245, 245) 37.8%,rgb(245, 245, 245) 48.8%,rgb(254, 254, 254) 53.1%,rgba(245, 245, 245, 0.75) 79.4%,rgba(245, 245, 245, 0.75) 84.3%);
    background-repeat: no-repeat;
}

.card-text {
	display: none;
    padding: 8px
}

.card-img-top{
    width: 300px;
    height: 200px;
    background-color: #fff;
    -webkit-transition: height 0.8s ease;
    transition: height 0.8s ease;
}

@media (max-width: 1120px){
    .bar-cont{
        width: 100%;
    }
    .news-block{
       max-width: 720px;
    }
    .card:hover{
      margin-right: -20px;
    }
    .card:hover .card-block{
      width: 300px;
    }
}

@media (max-width: 800px){
    .news-block{
       min-width: 380px;
    }
    .card:hover{
      border-left: 20px solid transparent !important;
      margin-right: -40px;
      -webkit-transform:  translate(0, -50px);
              transform:  translate(0, -50px);
    }
    .card:hover:before{
      -webkit-box-shadow: 0px 60px 40px RGBA(142, 142, 142, .5);
              box-shadow: 0px 60px 40px RGBA(142, 142, 142, .5);
    }
    .card:hover .card-block{
      width: 300px;
    }
}

@media (max-width: 580px){
  .news-block{
     max-width: 380px;
  }
}
/*-----------------
Pixel Grid
------------------*/
.pixel-grid{
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-size: cover;
  z-index: 15;
  opacity: .2;
  display: none;
}

#toggle-grid{
  position: fixed;
  top: 150px;
  right: 2px;
  z-index: 16;
  color: white;
  display: inline-block;
  /*border: 2px ridge white;*/
  font-size: 1.8em;
  width: 1.8em;
  text-align: center;
  line-height: 1.85em;
  background: #666;
  border-radius: 50%; /* or 50% width & line-height */
  opacity: .3;
  -webkit-transition: background 1s ease-out;
  transition: background 1s ease-out;
  display: none;
}

#toggle-grid:hover{
  cursor: pointer;
  background: repeating-linear-gradient(to bottom right,rgb(0, 0, 0) 0%,rgb(226, 226, 226) 61.4%,rgb(226, 226, 226) 66.9%,rgb(226, 226, 226) 76.6%,rgb(226, 226, 226) 88.7%,rgb(255, 255, 255) 100%);
}

.orange{
  color: orange !important;
  -webkit-box-shadow: 2px 2px 3px #888;
          box-shadow: 2px 2px 3px #888;
  opacity: 1 !important;
}

.activity-box p {
  word-wrap: break-word;
}

.activity-box{
  padding: 0 !important;
}

.activity-box > .content{
  padding: 30px 20px  !important;
}
.about-us-background{
    background-image: url(../assets/images/about-us.jpg);
}

.what-we-do-background{
    background-image: url('assets/images/what-we-do2.jpg')
 }

 .mission-background{
    background-image: url('assets/images/mission.png');
 }


 .our-vision-background{
    background-image: url('assets/images/vision.png');
 }
.background-cover{
    background-size: cover;
    background-repeat: no;
    box-shadow: 0 0 8px 8px rgba(243,245,250,.8) inset;
    webkit-box-shadow: 0 0 8px 8px rgba(243,245,250,.8) inset;
    -moz-box-shadow: 0 0 8px 8px rgba(243,245,250,.8) inset;
}
.color-white{
    color: #fff !important;
}
.height-250{
    height: 250px;
}


/*
.gradient-background-light{
    background-image: linear-gradient(to left , rgba(200,200,200,.9) , rgba(135,202,210,.8)) ,
                      url('assets/images/header/hero1.jpg');
}*/
.gradient-background-light{
  background-image: linear-gradient(to left , rgba(220,220,220,.95) , rgba(120,120,120,.95)) ,
                      url('assets/images/header/hero1.jpg') !important;
}

.gradient-background-dark{
    background-image: linear-gradient(to right , rgba(200,200,200,.9) , rgba(45,150,197 , .9)) ,
                      url('assets/images/header/hero4.jpg');
}


/*
section{
        border-bottom: 5px solid gray;
    }
*/
/*------------------*/
</style>

<style>

.upper-section {
  min-height: 50vh;
  width: 100%;
  background: #34495e;
}

.lower-section{
  min-height: 100px;
  width: 100%;
  background:  #2c3e50;
}
.section-divider {
  position: relative;
}
.section-divider:before {
      font-family: 'codepen';
      content: "Al Shather";
      width: 7rem;
      height: 6rem;
      background: rgb(45,150,197);
      border-radius: 100%;
      position: absolute;
      top: -3.25rem;
      left: calc(50% - 4.25rem);
      color: rgba(#2980b9, 0.5);
      line-height: 6rem;
      text-indent: 1.75rem;
      font-size: 1rem;
      font-weight: 700;
    }


</style>
@endsection

@section('content')

<section id="about" class="about bg-white">
    <div class="container" data-aos="fade-up">
      <div class="section-title">
        <h2 >{{ translate('About Us') }}</h2>
      </div>

      <div class="row content pm-4">
        <div class="col-lg-4 about-us-background background-cover">
        </div>
        <div class="col-lg-8 height-250">
          <p class="mt-4 text-style @if (Session::get('lang') == 'en') t-left @else t-right @endif" >
            {{translate('ALSHATHER Tarding Company, established in 2004, is one of best growing IT provider in the UAE and ME region . ALSHATHER Trading Company is proud of its primary asset, its work force, comprising of experienced & highly qualified management personnel, aggressive marketers .Groomed beneath its wings a well experienced, diversified, professional approaching, result oriented team . ALSHATHER Tarding Company contributes for your business with fast, convenient and cost effective solutions on the latest of Information Technology.')}}
        </p>

        </div>

      </div>

    </div>
  </section><!-- End About Us Section -->

  <!-- <div class="lower-section section-divider"> -->
<!------------------------>

<!-- ======= Activities Section ======= -->
<section id="activities" class="services section-bg bg-blue ">
    <div class="container" data-aos="fade-up">
      <div class="section-title">
        <h2>{{translate('Activities')}}</h2>
      </div>
      <div class="row">
        @if(count($activities) > 0)
                @foreach ($activities as $activity)
                <div class="col-xl-3 col-md-4 col-sm-6 col-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="100" >
                    <div class="icon-box activity-box" style="width: 100%">
                    <div class="underlay">
                        @for ($i=0 ; $i < count($activity->images) ; $i++)
                            <img src="{{$activity->images[$i]->path}}" alt="error" width="100%" height="150" data-index={{$i}}>
                        @endfor
                    </div>
                    <div class="content @if(Session::get('lang') == 'en') t-left @else t-right @endif">
                        <h4>
                            <a href="{{route('activity.show' ,$activity->id )}}">{{translate($activity->activity_name)}}</a></h4>
                        <p>
                            {{translate($activity->description)}}
                        </p>
                    </div>
                    </div>
                </div>
            @endforeach
        @else
           <div class="alert alert-danger center-text">{{translate('No Activities available')}}</div>
        @endif
      </div>
    </div>
</section><!-- End Activities Section -->

  <!-- ======= What We Do? Section ======= -->

<section id="wat-we-do" class="about bg-white">
  <div class="container" data-aos="fade-up">

    <div class="section-title">
      <h2 >{{translate('What We Do?')}}
      </h2>
    </div>

    <div class="row content">
      <div class="col-lg-6 col-md-6 height-250">
        <p class="text-style  @if (Session::get('lang') == 'en') t-left @else t-right @endif">
          {{translate('ALSHATHER Trading Company  provides both products and services to make them useful to any size of business. We are especially focused on providing Hardware / Software solutions, Networking and services to small, large size business. We offer a wide array of name brand hardware components for networks and computers backed by leading hardware manufactures around the world.')}}
        </p>
      </div>
      <div class="col-lg-6 col-md-6 what-we-do-background background-cover">
     </div>

    </div>

  </div>
</section><!-- End What We Do  Section -->

<!-- ======= What Our Mission Section ======= -->
<section id="our-mission" class="about bg-blue">
  <div class="container" data-aos="fade-up">

    <div class="section-title">
      <h2>
          {{translate('Our Mission')}}
      </h2>
    </div>

    <div class="row content">
        <div class="col-lg-4  mission-background background-cover">
        </div>

        <div class="col-lg-8 height-250">
            <p class="text-style @if (Session::get('lang') == 'en') t-left @else t-right @endif">
                {{translate('Specialize in providing Hardware and Networking solutions and sattalite internet provider and industrial laboratory to business and government markets whilst ensuring performance, reliability and efficiency to combine and collaborate in meeting mutual business expectations. Our goal is growing yearly in selling and services which make us double the value of the company yearly and increase our costumers and satisfy their demand by value and service.')}}
            </p>

        </div>

    </div>

  </div>
</section><!-- End our mission  Section -->

<!-- ======= What Our Vision Section ======= -->
<section id="our-vision" class="about bg-white">
  <div class="container" data-aos="fade-up">

    <div class="section-title">
      <h2>
          {{translate('Our Vision')}}
      </h2>
    </div>

    <div class="row content">
      <div class="col-lg-8 mt-4 height-250">
        <p class="text-style @if (Session::get('lang') == 'en') t-left @else t-right @endif">
          {{translate('We believe, "Customer Satisfaction Is the Driving Force Behind the Success of ALSHATHER Tarding Company , Attained Through Quality Driven Solutions". Our expertise, commitment and team effort guarantees customer satisfaction and paves the way to achieve our vision to be a "Market Leader in the providing computer industry".')}}
        </p>
      </div>
      <div class="col-lg-4 our-vision-background background-cover">
     </div>

    </div>

  </div>
</section><!-- End our vision  Section -->

<section id="brands" class="team section-bg bg-blue">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2>{{translate('Brands')}}</h2>
      </div>

        <!--========Cards========-->
            <div class="card-group row">
                @if (count($brands) > 0)
                    @foreach ($brands as $brand)

                    {{--
                   <div class="col-xl-3 col-md-4 col-sm-6  col-6 d-flex  mt-4 mb-4" data-aos="zoom-in" data-aos-delay="100" >
                        <div class="icon-box activity-box" style="margin: 0 10px;background-color: #fff;width:100%" >
                        <div class="underlay  d-flex justify-content-center align-items-center" style="padding: 5px;" >
                            @for ($i=0 ; $i < count($brand->images) ; $i++)
                                <img src="{{$brand->images[$i]->path}}" alt="error" width="100" height="50" data-index={{$i}}>
                            @endfor
                        </div>
                        <div class="content" style="height: 300px;">
                            <h4>
                                <a href="{{route('showCategories' , $brand->id)}}">{{$brand->brand_name}}</a>
                            </h4>
                            <hr>
                            <p>
                                {{$brand->description}}
                            </p>
                        </div>
                        </div>
                    </div>
                    --}}


                    <div class="col-md-3 col-sm-4 col-12 ">
                        <a href="{{route('showCategories' , $brand->id)}}">
                            <div class="underlay" style="cursor: pointer">
                                <div class="card" style="margin: 0 10px">
                                    <div class="card-img-top d-flex justify-content-center align-items-center" style="max-width: 100%;max-height: 100px;">
                                    @for ($i=0 ; $i < count($brand->images) ; $i++)
                                        <img src="{{$brand->images[$i]->path}}" alt="error" width="100" height="50" data-index={{$i}}>
                                    @endfor
                                    </div>
                                    <div class="card-block @if(Session::get('lang') == 'en') t-left @else t-right @endif"  >
                                        <h5 class="card-title" style="font-family: 'Anton', sans-serif;padding: 8px;font-weight:bold">{{translate($brand->brand_name)}}<hr></h5>
                                        <p class="card-text ">
                                            {{translate($brand->description)}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>


                @endforeach
                @else
                   <div class="alert alert-danger center-text">{{translate('No Brands Available')}}</div>
                @endif
            </div>
    </div>
</div>


<!-- Project section-->
<section id="projects" class="team section-bg bg-white">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2>{{translate('Our Projects')}}</h2>
      </div>

        <!--========Cards========-->
            <div class="row card-group">
                @if(count($projects) > 0)
                        @foreach ($projects as $project)
                        <div class="col-xl-3 col-md-4 col-xs-4 col-sm-6 col-12">
                             <div class="underlay" style="cursor: pointer">
                                <div class="card" style="margin: 0 10px">
                                    <div class="card-img-top" style="max-width: 100%;max-height: 100px;">
                                        @for ($i=0 ; $i < count($project->images) ; $i++)
                                            <img src="{{$project->images[$i]->path}}" alt="error" width="100%" height="100%" data-index={{$i}}>
                                        @endfor
                                    </div>
                                    <div class="card-block @if(Session::get('lang') == 'en') t-left @else t-right @endif" >
                                        <h5 class="card-title" style="font-family: 'Anton', sans-serif;padding: 8px;font-weight:bold">{{translate($project->project_name)}}<hr></h5>
                                            <p class="card-text">
                                                {{translate($project->description)}}
                                            </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                   <div class="alert alert-danger center-text">{{translate('No Projects Available')}}</div>
                @endif
            </div>
    </div>
</div>
</section>

<!-- ======= Clients Section ======= -->
<section id="clients" class="clients section-bg bg-blue mb-4">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
          <h2>{{translate('Our Clients')}}</h2>
        </div>

      <div class="row" data-aos="zoom-in">
        @if(count($clients) > 0)
           @foreach ($clients as $client)
              <div class="col-xl-2 col-md-3 col-sm-6 col-4 mb-4">
                    <div
                    class=" d-flex flex-column align-items-center justify-content-around"
                    style="background-color: white;padding: 4px;
                      box-shadow: 0 0 4px 4px rgba(243,245,250,.8) inset;
                      webkit-box-shadow: 0 0 4px 4px rgba(243,245,250,.8) inset;
                      -moz-box-shadow: 0 0 4px 4px rgba(243,245,250,.8) inset;">
                    <div style="width: 100%;height: 80px"
                    class="d-flex flex-row justify-content-center aligh-items-center">
                        <img src="{{url($client->images[0]->path)}}"  alt=""
                        style="filter:none;border-radius: 10px;max-height: 70px;max-width: 100px;width: 100%">
                    </div>

                    <div style="font-size: 20px;font-weight: 400" class="mt-2 pt-0">
                        {{$client->client_name}}
                    </div>
                </div>
              </div>
           @endforeach
        @else
        <div class="alert alert-danger center-text">{{translate('No Clients available')}}</div>
        @endif
      </div>

    </div>
  </section><!-- End Cliens Section -->


<!-- ======= About Us Section ======= -->

<!-- ======= Services Section ======= -->
 <section id="supports" class="services section-bg bg-white">
        <div class="container" data-aos="fade-up">

          <div class="section-title d-flex flex-column justify-content-center align-items-center" >
            <h2>{{translate('Support')}}</h2>
            <div  class="alert alert-info" style="width: 45%">
                {{translate('To Do this Operation your account must be verified')}}
            </div>

          </div>
          <div class="row d-flex justify-content-center">
            <div class="col-xl-3 col-md-6 d-flex align-items-stretch mb-4" data-aos="zoom-in" data-aos-delay="100">
                <div class="icon-box d-flex flex-column align-items-center">
                <div class="icon" >
                    <i class="fa fa-download" style="font-size:90px;"></i>
                </div>
                <h4>
                    <a href="@if(auth()->user() != null && auth()->user()->verified) /downloads/all @else #supports @endif">
                       <h3>{{translate('Downloads')}}</h3>
                    </a>
                </h4>
                <p class="@if (Session::get('lang') == 'en') t-left @else t-right @endif">
                    {{translate('Here you will find Brands datasheets,firmware,files and updates')}}
                </p>
              </div>
            </div>

            <div class="col-xl-3 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
                <div class="icon-box d-flex flex-column align-items-center">
                  <div class="icon" >
                      <i class="fa fa-ticket" style="font-size:90px;"></i>
                  </div>
                  <h4>
                      <a href="@if(auth()->user() != null && auth()->user()->verified) /ticket/create @else #supports @endif">
                        <h3>{{translate('Open Ticket')}}</h3>
                     </a>
                  </h4>
                  <p class="@if (Session::get('lang') == 'en') t-left @else t-right @endif">
                      {{translate('Al Shather Ticket System for Tech Support. Click to open new ticket and troubleshoot')}}
                  </p>
                </div>
              </div>


          </div>

        </div>
      </section><!-- End Services Section -->

<!-- end project section -->
    <!-- ======= Contact Section ======= -->
<section id="contact" class="contact bg-blue">
        <div class="container" data-aos="fade-up">

          <div class="section-title">
            <h2>{{translate('Contact')}}</h2>
          </div>

          <div class="row">

            <div class="col-lg-6 mb-4">
              <div class="info">
                 <h2 class="country_contact">{{translate('UAE')}}</h2>
                <div class="address">
                  <i class="bi bi-geo-alt"></i>
                  <h4>Location:</h4>
                  <p>JAFZA ONE BUILDING/TOWER A ,
                    OFFICE 1184<br>
                    JABEL ALI FREE ZONE<br>
                    PO Box 46846, Dubai, U.A.E.</p>
                </div>

                <div class="email">
                  <i class="bi bi-envelope"></i>
                  <h4>Email:</h4>
                  <p>shather@emirates.net.ae</p>
                </div>

                <div class="phone">
                  <i class="bi bi-phone"></i>
                  <h4>Tel:</h4>
                  <p>+971 4 3518188</p>
                </div>

                <div class="fax">
                    <i class="bi bi-printer"></i>
                    <h4>Fax:</h4>
                    <p>+971 4 3518189</p>
                  </div>
              </div>
            </div>
            <div class="col-lg-6 mb-4">
                <div class="info" >
                    <h2 class="country_contact">{{translate('Iraq')}}</h2>
                    <div class="address">
                      <i class="bi bi-geo-alt"></i>
                      <h4>Location:</h4>
                      <p>Baghdad - Iraq, Sena'a St.<br>
                        Opp. University of Technology<br><br></p>
                    </div>

                    <div class="email">
                      <i class="bi bi-envelope"></i>
                      <h4>Email:</h4>
                      <p>shathe1@emirates.net.ae</p>
                    </div>

                    <div class="phone">
                      <i class="bi bi-phone"></i>
                      <h4>Tel 1:</h4>
                      <p>+964 1 7198706</p>
                    </div>

                    <div class="phone">
                        <i class="bi bi-phone"></i>
                        <h4>Tel 2:</h4>
                        <p>+964 1 7171383</p>
                      </div>
                  </div>
            </div>
          </div>
        </div>
      </section><!-- End Contact Section -->


      @endsection

@section('scripts')

<script src="jquery-3.1.1.min.js"></script>
<script name="toggle-grid" type="text/javascript">
$(document).ready(function(){
  $(document).on("keypress", function(event) {
    // If 'alt + g' keys are pressed:
    if (event.which === 169){
        $('#toggle-grid').toggle();
     }
  });

  $('#toggle-grid').on("click"
  , function() {
      $('.pixel-grid').toggle();
      $('#toggle-grid').toggleClass('orange');
    });
});

var main = function () {
    $('.push-bar').on('click', function(event){
      if (!isClicked){
        event.preventDefault();
        $('.arrow').trigger('click');
        isClicked = true;
      }
    });

    $('.arrow').css({
      'animation': 'bounce 2s infinite'
    });
    $('.arrow').on("mouseenter", function(){
        $('.arrow').css({
                'animation': '',
                'transform': 'rotate(180deg)',
                'background-color': 'black'
           });
    });
     $('.arrow').on("mouseleave", function(){
        if (!isClicked){
            $('.arrow').css({
                    'transform': 'rotate(0deg)',
                    'background-color': 'black'
               });
        }
    });

    var isClicked = false;

    $('.arrow').on("click", function(){
        if (!isClicked){
            isClicked = true;
            $('.arrow').css({
                'transform': 'rotate(180deg)',
                'background-color': 'black',
           });

            $('.bar-cont').animate({
                top: "-15px"
            }, 300);
            $('.main-cont').animate({
                top: "0px"
            }, 300);
            // $('.news-block').css({'border': '0'});
            // $('.underlay').slideDown(1000);

        }
        else if (isClicked){
            isClicked = false;
            $('.arrow').css({
                'transform': 'rotate(0deg)',       'background-color': 'black'
           });

            $('.bar-cont').animate({
                top: "-215px"
            }, 300);
            $('.main-cont').animate({
                top: "-215px"
            }, 300);
        }
    console.log('isClicked= '+isClicked);
    });

    $('.card').on('mouseenter', function() {
     $(this).find('.card-text').slideDown(1000);
    });

    $('.card').on('mouseleave', function(event) {
       $(this).find('.card-text').css({
         'display': 'none'
       });
     });

};

$(document).ready(main);
</script>

<script src="{{url('assets/js/carousel.js')}}"></script>
@endsection
