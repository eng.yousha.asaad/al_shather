@extends('pages.index')

@section('page_title')
Sub Activities
@endsection

@section('styles')
<style>

.img-wrapper
{
	width: 300px;
	height: 350px;
	position: relative;
	overflow: hidden;
}

.img-wrapper:before
{
	content: '';
	position: absolute;
	top: 0;
	left: 180%;
	height: 100%;
	width: 100%;
	background: rgba(255,255,255,.3);
	z-index: 1;
	transform: skew(45deg);
	transition: .5s;
}

.img-wrapper:hover:before
{
	left: -180%;
}

.img-wrapper img
{
	height: 400px;
	width: 300px;
	filter: grayscale(100%);
	transition: 2s;
}
.img-wrapper:hover img
{
	filter: grayscale(0%);
	transform: scale(1.1);
}

.img-wrapper h2
{
	background: tomato;
	font-family: Poppins;
	color: #fff;
	text-align: center;
	text-transform: uppercase;
	margin: 0;
	padding: 10px 0;
    width: 100%;
	position: absolute;
	bottom: 0;
	transform: perspective(400px) rotateY(90deg);
	transform-origin: right;
	transition: 1s;
}

.img-wrapper:hover h2
{
	transform: perspective(400px) rotateY(0deg);
}


.img-wrapper .decription
{
    position: absolute;
	top: 0;
	left: 0;
	margin: 0;
    width: 100%;
	padding: 8px;
	background: rgba(0,0,0,.5);
	text-align: left;
	transform: perspective(800px) rotateY(90deg);
	transition: .5s;
	transform-origin: left;
}

.img-wrapper:hover .decription
{
	transform: perspective(800px) rotateY(0deg);
}

.img-wrapper:hover .decription
{
	transition-delay: .2s;
}

.decription{
    color: white;
    width: 100%;
}
</style>

@endsection

@section('content')
<section id="sub_activities" class="services section-bg">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2>{{$activityname}}  Activities</h2>
      </div>
      <div class="row">
          @foreach ($activities as $act)
                <div class="image-area col-xl-3 col-md-3 d-flex align-items-stretch mt-4">
                    <div class="img-wrapper">
                        <div class="underlay">
                            @for ($i=0 ; $i < count($act->images) ; $i++)
                                 <img src="{{$act->images[$i]->path}}" alt="error"  data-index={{$i}}>
                            @endfor
                        </div>
                        <h2>{{$act->sub_activity_name}}</h2>
                        <div class="decription">
                            {{$act->description}}
                        </div>
                    </div>
                </div>
          @endforeach
      </div>


    </div>
</section>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            window.location.href = '#sub_activities';
        });
    </script>
    <script src="{{url('assets/js/carousel.js')}}"></script>
@endsection
