@extends('pages.index')

@section('page_title')
Services
@endsection

@section('styles')

<style>
    .skill{
        padding: 8px !important;
    }

</style>
@endsection

@section('content')
    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us section-bg bg-blue">
        <div class="container-fluid" data-aos="fade-up">
            <div class="section-title">
                <h3>{{translate('Project Planning')}}</h3>
              </div>
          <div class="row">
            <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">
              <div class="content">
                <!-- <h3><strong>Project Planning</strong></h3> -->
                <p>
                </p>
              </div>

              <div class="accordion-list">
                <ul>
                  <li >
                    <a data-bs-toggle="collapse" class="collapse d-flex @if(Session::get('lang') == 'en') flex-row @else flex-row-reverse @endif" data-bs-target="#accordion-list-1">
                        <span class="ml-2 mr-2">01</span>
                        {{translate('Resource planning')}}
                    </a>
                  </li>
                  <li>
                    <a data-bs-toggle="collapse" class="collapse d-flex @if(Session::get('lang') == 'en') flex-row @else flex-row-reverse @endif" data-bs-target="#accordion-list-1">
                        <span class="ml-2 mr-2">02</span>
                        {{translate('budgeting and execution of the projects identified deliverables')}}
                    </a>
                  </li>
                  <li>
                    <a data-bs-toggle="collapse" class="collapse d-flex @if(Session::get('lang') == 'en') flex-row @else flex-row-reverse @endif" data-bs-target="#accordion-list-1">
                        <span class="ml-2 mr-2">03</span>
                        {{translate('Quality assurance')}}
                    </a>
                  </li>
                  <li>
                    <a data-bs-toggle="collapse" class="collapse d-flex @if(Session::get('lang') == 'en') flex-row @else flex-row-reverse @endif" data-bs-target="#accordion-list-1">
                        <span class="ml-2 mr-2">04</span>
                        {{translate('User acceptance testing')}}
                    </a>
                  </li>
                  <li>
                    <a data-bs-toggle="collapse" class="collapse d-flex @if(Session::get('lang') == 'en') flex-row @else flex-row-reverse @endif" data-bs-target="#accordion-list-1">
                        <span class="ml-2 mr-2">05</span>
                        {{translate('Documentation and Training')}}
                    </a>
                  </li>
                </ul>
              </div>

            </div>

            <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style='background-image: url("assets/img/why-us.png");' data-aos="zoom-in" data-aos-delay="150">&nbsp;</div>
          </div>

        </div>
    </section><!-- End Why Us Section -->

      <!-- ======= Customized Development Section ======= -->
    <section id="customized-development" class="customized-developmen bg-white">
        <div class="container" data-aos="fade-up">

          <div class="section-title">
            <h2>
                {{translate('Customized Development')}}
            </h2>
          </div>

          <div class="row content">
            <div class="col-lg-12">
              <p class="text-style @if (Session::get('lang') == 'en') t-left @else t-right @endif" >
                {{translate('Provision of customized application development that will utilize appropriate skills to ensure best quality, effective and efficient solution in accordance with requirement. We also customize our off the shelf packages for our customers.')}}
              </p>
            </div>
          </div>
        </div>
    </section><!-- End Customized Development Section -->

    <!------------------------>
    <section id="skills" class="skills bg-blue">
        <div class="container" data-aos="fade-up">
            <div class="section-title">
                <h2>{{translate('Network Designing and Integration')}}
                </h2>
            </div>
            <div class="row">
            <div class="col-lg-4 d-flex align-items-center" data-aos="fade-right" data-aos-delay="100">
              <img src="assets/img/skills.png" class="img-fluid" alt="">
            </div>
            <div class="col-lg-8 pt-4 pt-lg-0 content" data-aos="fade-left" data-aos-delay="100">
              <!-- <h3>Voluptatem dignissimos provident quasi corporis voluptates</h3> -->
              <p class="fst-italic @if (Session::get('lang') == 'en') t-left @else t-right @endif">
                {{translate('ALSHATHER TRADING COMPANY provides quality and professional services in the areas of high-end networking, we design, install & implement turnkey solutions based on current trends plus keeping an eye on emerging technologies. We conduct:')}}
              </p>

              <div class="skills-content">

                <div class="progress">
                  <span class="skill">Project Management </span>
                  <div class="progress-bar-wrap">
                    <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>

                <div class="progress">
                  <span class="skill">Site Preparation - raised Floors</span>
                  <div class="progress-bar-wrap">
                    <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>


                <div class="progress">
                  <span class="skill">UTP/ FTP/ Fiber</span>
                  <div class="progress-bar-wrap">
                    <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>

                <div class="progress">
                    <span class="skill">Firewall & Security Deployment</span>
                    <div class="progress-bar-wrap">
                      <div class="progress-bar" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>

                <div class="progress">
                    <span class="skill">LAN-Ethernet, Fast Ethernet, Giga Ethernet</span>
                    <div class="progress-bar-wrap">
                      <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>

                <div class="progress">
                    <span class="skill">ATM</span>
                    <div class="progress-bar-wrap">
                      <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>

                <div class="progress">
                    <span class="skill">Remote Access Solutions - X.25</span>
                    <div class="progress-bar-wrap">
                      <div class="progress-bar" role="progressbar" aria-valuenow="89" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>

                <div class="progress">
                    <span class="skill">Frame Relay</span>
                    <div class="progress-bar-wrap">
                      <div class="progress-bar" role="progressbar" aria-valuenow="99" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>

                <div class="progress">
                    <span class="skill">ATM, ISDN, Banking Solutions, Wireless Solutions - WAN and LAN</span>
                    <div class="progress-bar-wrap">
                      <div class="progress-bar" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>

              </div>

            </div>
          </div>

        </div>
      </section><!-- End Skills Section -->


    <!-- ======= Cta Section ======= -->
    <section id="cta" class="cta">
        <div class="container" data-aos="zoom-in">

          <div class="row">
            <div class="col-lg-9 text-center text-lg-start">
              <h3>
                  {{translate('Maintenance and support')}}
             </h3>
              <p>
              {{translate('ALSHATHER TRADING COMPANY meets all your computer maintenance needs! In today\'s competitive computer market, you need a world class maintenance partner committed to your success. We proven track of computer maintenance service. We have learned to listen to our customers\' needs and have strived to exceed their expectations in getting the job done. We are committed to the ongoing education of all of our service technicians and support staff to ensure the expertise necessary to handle all hardware, software and network requirements.')}}
              </p>
            </div>
            <div class="col-lg-3 cta-btn-container text-center">
              <a class="cta-btn align-middle" href="/home#contact">{{translate('Call To Action')}}</a>
            </div>
          </div>

        </div>
      </section><!-- End Cta Section -->

      @endsection

@section('scripts')
@endsection
