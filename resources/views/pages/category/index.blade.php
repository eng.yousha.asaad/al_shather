@extends('pages.index')

@section('page_title')
Categories
@endsection

@section('styles')
<style>

    a{
        cursor: pointer;
    }
    .image-item{
        max-width: 150px;
        width: 150px;
        height: 80px;
        margin: 8px;
        border-radius: 10px;
        overflow: hidden;
        cursor: pointer;
    }

    .image-item  img{
        max-width: 100%;
        height: auto;
        object-fit: cover;
        display: block;
    }

    .overlay{
        width: 100%;
        height:100%;
        position: absolute;
        top:0;
        opacity:0;
        background-color: #fff;
        display: flex;
        justify-content: center;
        align-items: center;
    }




    .image-item:hover  .overlay{
        opacity: 0.5;
    }

    .image-box{
        display: flex;
        justify-content: flex-start;
        flex-wrap: wrap;
        width:100%;
    }

   .delete-image-btn{
       background-color: transparent;
       border: none;
       color: red;
   }

   .preview-image-btn > i{
       font-size: 25px !important;
       color: blue !important;
    }
</style>

<script>
    $(document).ready(function(){
        // Activate tooltip
        $('[data-toggle="tooltip"]').tooltip();

        // Select/Deselect checkboxes
        var checkbox = $('table tbody input[type="checkbox"]');
        $("#selectAll").click(function(){
            if(this.checked){
                checkbox.each(function(){
                    this.checked = true;
                });
            } else{
                checkbox.each(function(){
                    this.checked = false;
                });
            }
        });
        checkbox.click(function(){
            if(!this.checked){
                $("#selectAll").prop("checked", false);
            }
        });
    });
    </script>


@endsection

@section('content')
<div class="container-fluid">
	<div class="table-responsive">
		<div class="table-wrapper">
			<div class="table-title">
                <div class="row">

                    <div class="col-sm-6">
						<h2>Manage <b>Categories</b></h2>
					</div>

                    <div class="col-sm-6">
						<button id="add_modal_btn"     class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Add New Category</span></button>
						<button id="delete_categories_btn" class="btn btn-danger" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Delete</span></button>
					</div>

                </div>
			</div>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>
							<span class="custom-checkbox">
								<input type="checkbox" id="selectAll">
								<label for="selectAll"></label>
							</span>
						</th>
						<th>Name</th>
						<th>Description</th>
                        <th>Brand</th>
                        <th>images</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
				  @if(count($categories) > 0)
                      @foreach ($categories as $category)
                        <tr>
                            <td width="10%">
                                <span class="custom-checkbox">
                                    <input type="checkbox" id="checkbox{{$category->id}}" name="options[]" value="{{$category->id}}">
                                    <label for="checkbox{{$category->id}}"></label>
                                </span>
                            </td>
                            <td width="15%">{{$category->category_name}}</td>
                            <td width="15%">{{$category->description}}</td>
                            <td width="15%" id="{{$category->brand->id}}">{{$category->brand->brand_name}}</td>
                            <td width="35%" >
                                <div class="image-box">
                                    @if(count($category->images) > 0)
                                        @foreach ( $category->images as $image)
                                            <div class="image-item" style="position: relative">
                                                <img src="{{$image->path}}" alt="error im image">
                                                <div class="overlay">
                                                    <a  class="preview-image-btn" href="{{$image->path}}" target="_blank">
                                                        <i class="material-icons" data-toggle="tooltip" title="Preview image">&#xe417;</i>
                                                    </a>

                                                    <form  method="post" action="{{route('image.destroy' , [$image->id])}}" >
                                                        @csrf
                                                        @method('delete')
                                                        <button type="submit" class="material-icons delete-image-btn" data-toggle="tooltip" title="Delete image">&#xE872;</button>
                                                    </form>

                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                      <div>
                                          No images avilable for this Category
                                      </div>
                                    @endif
                                </div>
                            </td>
                            <td width="10%">
                                <a class="edit"  data-category_id="{{$category->id}}" data-toggle="modal">
                                    <i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i>
                                </a>
                                <a class="delete" data-toggle="modal">
                                <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                                <form method="post" action="category/{{$category->id}}">
                                    @method('delete')
                                    @csrf
                                </form>
                            </td>
                        </tr>
                      @endforeach
                  @else
                     <tr style="text-align: center">
                         <td colspan="6">No Data Avilable</td>
                     </tr>
                  @endif

				</tbody>
			</table>
        <!--
			<div class="clearfix">
				<div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>
				<ul class="pagination">
					<li class="page-item disabled"><a href="#">Previous</a></li>
					<li class="page-item"><a href="#" class="page-link">1</a></li>
					<li class="page-item"><a href="#" class="page-link">2</a></li>
					<li class="page-item active"><a href="#" class="page-link">3</a></li>
					<li class="page-item"><a href="#" class="page-link">4</a></li>
					<li class="page-item"><a href="#" class="page-link">5</a></li>
					<li class="page-item"><a href="#" class="page-link">Next</a></li>
				</ul>
			</div>
        -->
		</div>
	</div>
</div>
<!-- Add Modal HTML -->
@include('modals.category.addCategory')
<!-- Edit Modal HTML -->
@include('modals.category.editCategory')
<!-- Delete Modal HTML -->
@include('modals.category.deleteCategory')

<form action="delete_categories"  method="post" id="delete_categories_form">

</form>
@endsection

@section('scripts')

<script>
    $(document).ready(function(){

        $('#add_modal_btn').click(function(){
            $('#addCategoryModal').modal('show');
        });

        $('.edit').click(function(){

            let category = $(this).parent().parent().children();

            let c_name = category[1].textContent;
            let c_description = category[2].textContent;
            let c_brand_id = category[3].id;

            let c_id = $(this).attr('data-category_id');

            $('#editCategoryName').val(c_name);
            $('#editcategoryDescription').val(c_description);
            $('#editBrand').val(c_brand_id);
            $('#editCategoryModal form').attr('action' , `category/${c_id}`);

            $('#editCategoryModal').modal('show');
        });


        $('#delete_categories_btn').click(function(){
            $('#delete_categories_form').empty();

            var csrfVar = $('meta[name="csrf-token"]').attr('content');

            $("#delete_categories_form").append("<input name='_token' value='" + csrfVar + "' type='hidden'>");

            $('table tbody input[type="checkbox"]').each(function(){
                 if($(this).is(":checked"))
                 {
                    $('#delete_categories_form').append(`<input type="hidden" value="${$(this).val()}"  name="del_${$(this).val()}"/>`);
                 }
           });

          $('#delete_categories_form').submit();
        });


        $('.delete').click(function(){
            let form = $(this).parent().children().last();
            form.submit();
        });
    });
</script>
@endsection



