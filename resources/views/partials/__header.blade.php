<!-- ======= Header ======= -->
  <header id="header" class="fixed-top"
  style="background: rgba(240,240,240,.95) ">
    <div class=" d-flex align-items-center justify-content-between" style="padding: 0 25px;">

      <h1 class="logo">
       <img src="{{url('assets/images/logo3.png')}}" alt="error in logo" width="200" height="200" >
      </h1>

      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo me-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav id="navbar" class="navbar align-self-end">
        <ul>
          <li><a class="nav-link scrollto active" href="/home">{{translate('Home')}}</a></li>
          <li><a class="nav-link scrollto" href="/home#brands">{{translate('Brands')}}</a></li>
        <!--  <li><a class="nav-link scrollto" href="/home#activities">Activities</a></li>-->
          <!--<li><a class="nav-link scrollto" href="/home#projects">Projects</a></li>-->
          <li><a class="nav-link scrollto" href="/home#clients">{{translate('Clients')}}</a></li>
          <li><a class="nav-link scrollto" href="/services">{{translate('Services')}}</a></li>
          <li><a class="nav-link scrollto" href="/home#about">{{translate('About Us')}}</a></li>
          <li><a class="nav-link scrollto" href="/home#contact">{{translate('Contact')}}</a></li>
          <li><a class="nav-link   scrollto" href="/home#supports">{{translate('Support')}}</a></li>

           @if (auth()->user())
              @if(auth()->user()->role == 'admin')
                    <li class="dropdown">
                    <a href="#">
                        <span>{{translate('Administration')}}</span>
                        <i class="bi bi-chevron-down"></i>
                    </a>
                    <ul>
                    <li><a href="{{route('brand.index')}}">{{translate('Brands')}}</a></li>
                    <li><a href="{{route('client.index')}}">{{translate('Clients')}}</a></li>
                    <li><a href="{{route('category.index')}}">{{translate('Categories')}}</a></li>
                    <li><a href="{{route('product.index')}}">{{translate('Products')}}</a></li>
                    <li><a href="{{route('project.index')}}">{{translate('Projects')}}</a></li>
                    <li class="dropdown">
                        <a href="#">
                            <span>{{translate('Activities')}}</span>
                            <i class="bi bi-chevron-right"></i>
                        </a>
                    <ul>
                        <li><a href="{{route('activity.index')}}">{{translate('Main Activities')}}</a></li>
                        <li><a href="{{route('sub-activity.index')}}">{{translate('Sub Acticity')}}</a></li>
                    </ul>
                    </li>
                    <li><a href="{{route('ticket.index')}}">{{translate('Tickets')}}</a></li>
                    <li><a href="{{route('file.index')}}">{{translate('Downloads')}}</a></li>
                    <li class="d-flex flex-row justify-content-between align-items-center">
                        <a href="{{route('user.index')}}">{{translate('Join Requests')}}</a>
                        <span class="request-number">{{count($users_requests)}}</span>
                    </li>
                    <li><a href="{{route('sentence.index')}}">{{translate('Translation')}}</a></li>
                    <li><a href="{{route('general_setting')}}">{{translate('General Setting')}}</a></li>

                    </ul>
                </li>
              @endif
           @endif
          <!-- authentication -->
          <li class="dropdown">

            <!-- Authentication Links -->
            @guest
            <a href="#">
                <span>{{translate('Account')}}</span>
                <i class="bi bi-chevron-down"></i>
            </a>
            <ul class="navbar-nav ms-auto">
                @if (Route::has('login'))
                    <li><a href="{{ route('login') }}">{{ translate('Login') }}</a></li>
                @endif

                @if (Route::has('register'))
                    <li><a  href="{{ route('register') }}">{{ translate('Register') }}</a></li>
                @endif
            </ul>
            @else
                <a  href="#" role="button"
                 data-bs-toggle="dropdown"
                 aria-haspopup="true"
                 aria-expanded="false" v-pre
                 style="color: cyan;font-size:20px;"
                 class="d-flex  @if(Session::get('lang') == 'en') flex-row @else flex-row-reverse @endif flex-start"
                 >
                 <span style="padding: 2px">{{translate('Hello')}}</span>

                 <span style="padding: 2px">{{Auth::user()->name }}</span>

                </a>
                <ul class="navbar-nav ms-auto">
                    <li>
                        <div >
                            <a  href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                {{ translate('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            @endguest
        </li>
        </ul>

        <i class="bi bi-list mobile-nav-toggle">

        </i>


      </nav><!-- .navbar -->
      <a href="/change_language?lang=@if(Illuminate\Support\Facades\Session::get('lang') == 'en') ar @else en @endif" class="btn btn-info" >
        @if(Session::get('lang') == 'en')
           عربي
        @else
          English
        @endif
    </a>
    </div>
  </header>
  <!-- End Header -->


  @if(auth()->user() && !auth()->user()->verified)
    <div class="col-md-6 alert alert-danger fixed-top verify-account-style @if(Session::get('lang') == 'en') t-left @else t-right @endif">
        {{translate('Your Account has not been verified yet.Please Wait util Admin Verify your request')}}
    </div>
  @endif


