  <!-- ======= Footer ======= -->

  <footer id="footer" class="gradient-background-light">


    <div class="footer-top">
      <div class="container">
          <div class="row d-flex justify-content-center">
            <div class="col-lg-3 col-md-6 footer-contact">
                <img src="{{url('assets/images/logo3.png')}}" alt="error in logo" width="200" height="40" >
              </div>
          </div>
        <div class="row">

          <div class="col-lg-4 col-md-4 col-sm-6 col-6 footer-links">
            <h4>{{translate('About Company')}}</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="/home">{{translate('Home')}}</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="/home#about">{{translate('About Us')}}</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="/home#contact">{{translate('Contact')}}</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="/home#our-mission">{{translate('Our Mission')}}</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="/home#our-vision">{{translate('Our Vision')}}</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="/home#support">{{translate('Support')}}</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-4 col-sm-6 col-6 footer-links">
            <h4>{{translate('Our Services')}}</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="/home#brands">{{translate('Brands')}}</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="/home#activities">{{translate('Activities')}}</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="/home#projects">{{translate('Projects')}}</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="/home#clients">{{translate('Clients')}}</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="/services">{{translate('Services')}}</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-4 col-sm-12 col-12 footer-links @if (Session::get('lang') == 'en') t-left @else t-right @endif">
            <h4>{{translate('Our Social Networks')}}</h4>
            <p>{{translate('You can Follow us and listen for each update via our social networks accounts')}}</p>
            <div class="social-links mt-3">
              <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
              <a href="https://www.facebook.com/ShatherBaghdad" class="facebook" target="_blank"><i class="bx bxl-facebook"></i></a>
              <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
              <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
          </div>

        </div>
      </div>
    </div>
  </footer>

