  <!-- ======= Hero Section ======= -->



  <section id="hero" class="d-flex align-items-center">
    <div class="underlay fit-header">
        <img src="{{url('assets/images/header/hero1.jpg')}}" alt="" width="100%" height="100%" data-index="0">
        <img src="{{url('assets/images/header/hero2.jpg')}}" alt="" width="100%" height="100%" data-index="1">
        <img src="{{url('assets/images/header/hero3.jpg')}}" alt="" width="100%" height="100%" data-index="2">
        <img src="{{url('assets/images/header/hero4.jpg')}}" alt="" width="100%" height="100%" data-index="3">
    </div>

    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
         <!-- <h1>Better Solutions For Your Business</h1>
          <h2>We are team of talented designers making websites with Bootstrap</h2>
         -->
         <!--
          <div class="d-flex justify-content-end justify-content-lg-start">
            <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="glightbox btn-get-started"><i class="bi bi-play-circle"></i><span>Watch Video</span></a>
          </div>
        -->
        </div>

      </div>
    </div>
  </section>
  <!-- End Hero -->
