<div id="addProjectModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST"  action="{{route('project.store')}}"  enctype="multipart/form-data">
                @csrf
				<div class="modal-header">
					<h4 class="modal-title">Add Project</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="addProjectName">Project Name</label>
						<input id="addProjectName" type="text" class="form-control" name="project_name" maxlength="40" required>
					</div>

                     <div class="form-group">
                        <label for="addimge" class="form-label">Project image</label>
                        <input class="form-control form-control-md" name="images[]" multiple id="addimge" type="file" accept="image/*" required>
                      </div>

					<div class="form-group">
						<label for="projectDescription">Project Description</label>
						<textarea id="projectDescription" class="form-control" name="description" required required></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-info" data-dismiss="modal" value="Cancel">
					<input type="submit" class="btn btn-success" value="Add">
				</div>
			</form>
		</div>
	</div>
</div>
