<div id="editCategoryModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST"  enctype="multipart/form-data">
                @csrf
                @method('put')
				<div class="modal-header">
					<h4 class="modal-title">Edit Category</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="editCategoryName">Category Name</label>
						<input id="editCategoryName" type="text" class="form-control" name="category_name" maxlength="40" required>
					</div>

                     <div class="form-group">
                        <label for="editimge" class="form-label">Extra Category image</label>
                        <input class="form-control form-control-md" name="images[]" multiple id="editimge" type="file" accept="image/*">
                      </div>

                      <div class="form-group">
                        <label for="editBrand" class="form-label">Brand Name</label>
                        <select class="form-select selectpicker" id="editBrand"  name="brand_id" required>
                            <option data-hidden="true" value="">
                                Select Category's Brand
                            </option>
                            @foreach($brands as $brand)
                              <option value="{{$brand->id}}">{{$brand->brand_name}}</option>
                            @endforeach

                          </select>
                      </div>

					<div class="form-group">
						<label for="editcategoryDescription">Category Description</label>
						<textarea id="editcategoryDescription" class="form-control" name="description" required required></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-info" data-dismiss="modal" value="Cancel">
					<input type="submit" class="btn btn-success" value="Edit">
				</div>
			</form>
		</div>
	</div>
</div>
