<div id="addCategoryModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST"  action="{{route('category.store')}}"  enctype="multipart/form-data">
                @csrf
				<div class="modal-header">
					<h4 class="modal-title">Add Category</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="addCategoryName">Category Name</label>
						<input id="addCategoryName" type="text" class="form-control" name="category_name" maxlength="40" required>
					</div>

                     <div class="form-group">
                        <label for="addimge" class="form-label">Category image</label>
                        <input class="form-control form-control-md" name="images[]" multiple id="addimge" type="file" accept="image/*" required>
                      </div>

                      <div class="form-group">
                        <label for="addbrand" class="form-label">Brand Name</label>
                        <select class="form-select selectpicker"  name="brand_id"  id="addbrand"  required>
                            <option data-hidden="true" value="">
                                Select Category's Brand
                            </option>
                            @foreach($brands as $brand)
                              <option value="{{$brand->id}}">{{$brand->brand_name}}</option>
                            @endforeach

                          </select>
                      </div>

					<div class="form-group">
						<label for="categoryDescription">Category Description</label>
						<textarea id="categoryDescription" class="form-control" name="description" required required></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-info" data-dismiss="modal" value="Cancel">
					<input type="submit" class="btn btn-success" value="Add">
				</div>
			</form>
		</div>
	</div>
</div>
