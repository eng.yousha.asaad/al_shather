<div id="editFileModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
				<div class="modal-header">
					<h4 class="modal-title">Edit file</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">

                    <div class="form-group">
						<label for="editFileName">file Name</label>
						<input id="editFileName" type="text" class="form-control" name="file_name" maxlength="40" required>
					</div>

                    <div class="form-group">
						<label for="editfFileType">File Type</label>
                        <select  id="editfFileType" class="custom-select" name="type" required>
                            <option value="" hidden>Select File Type</option>
                            <option value="Datasheet">Datasheet</option>
                            <option value="Training File">Training File</option>
                            <option value="QIG&User Manual">QIG&User Manual</option>
                            <option value="Firmware">Firmware</option>
                            <option value="Tool">Tool</option>
                        </select>
					</div>

                     <div class="form-group">
                        <label for="editFilePath" class="form-label">Change File</label>
                        <div class="alert alert-warning">If you want to keep current file, do not choose anything</div>
                        <input class="form-control form-control-md" name="file" id="editFilePath" type="file">
                    </div>

					<div class="form-group">
						<label for="editfileDescription">file Description</label>
						<textarea id="editfileDescription" class="form-control" name="description" required required></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
					<input type="submit" class="btn btn-info" value="Save">
				</div>
			</form>
		</div>
	</div>
</div>
