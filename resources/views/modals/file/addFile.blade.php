<div id="addFileModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST"  action="{{route('file.store')}}"  enctype="multipart/form-data">
                @csrf
				<div class="modal-header">
					<h4 class="modal-title">Add File</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="addfFileName">File Name</label>
						<input id="addfFileName" type="text" class="form-control" name="file_name" maxlength="40" required>
					</div>

                    <div class="form-group">
						<label for="addfFileType">File Type</label>
                        <select  id="addfFileType" class="custom-select" name="type" required>
                            <option value="" hidden>Select File Type</option>
                            <option value="Datasheet">Datasheet</option>
                            <option value="Training File">Training File</option>
                            <option value="QIG&User Manual">QIG&User Manual</option>
                            <option value="Firmware">Firmware</option>
                            <option value="Tool">Tool</option>
                        </select>
					</div>

                     <div class="form-group">
                        <label for="addFilePath" class="form-label">Choose File</label>
                        <input class="form-control form-control-md" name="file" id="addFilePath" type="file"  required>
                      </div>

					<div class="form-group">
						<label for="filetDescription">File Description</label>
						<textarea id="fileDescription" class="form-control" name="description" required required></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-info" data-dismiss="modal" value="Cancel">
					<input type="submit" class="btn btn-success" value="Add">
				</div>
			</form>
		</div>
	</div>
</div>
