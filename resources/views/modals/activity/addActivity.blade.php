<div id="addActivityModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST"  action="{{route('activity.store')}}"  enctype="multipart/form-data">
                @csrf
				<div class="modal-header">
					<h4 class="modal-title">Add Activity</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="addActivityName">Activity Name</label>
						<input id="addActivityName" type="text" class="form-control" name="activity_name" maxlength="40" required>
					</div>

                     <div class="form-group">
                        <label for="addimge" class="form-label">Activity image</label>
                        <input class="form-control form-control-md" name="images[]" multiple id="addimge" type="file" accept="image/*" required>
                      </div>

					<div class="form-group">
						<label for="activityDescription">Activity Description</label>
						<textarea id="activityDescription" class="form-control" name="description" required required></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-info" data-dismiss="modal" value="Cancel">
					<input type="submit" class="btn btn-success" value="Add">
				</div>
			</form>
		</div>
	</div>
</div>
