<div id="editActivityModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
				<div class="modal-header">
					<h4 class="modal-title">Edit Activity</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
                    <div class="form-group">
						<label for="editActivityName">Activity Name</label>
						<input id="editActivityName" type="text" class="form-control" name="activity_name" maxlength="40" required>
					</div>

                     <div class="form-group">
                        <label for="editimge" class="form-label">Activity image</label>
                        <input class="form-control form-control-md" name="images[]" multiple id="editimge" type="file" accept="image/*">
                      </div>

					<div class="form-group">
						<label for="editActivityDescription">Activity Description</label>
						<textarea id="editActivityDescription" class="form-control" name="description" required required></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
					<input type="submit" class="btn btn-info" value="Save">
				</div>
			</form>
		</div>
	</div>
</div>
