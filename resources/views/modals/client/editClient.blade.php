<div id="editClientModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
				<div class="modal-header">
					<h4 class="modal-title">Edit Client</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
                    <div class="form-group">
						<label for="editClientName">Client Name</label>
						<input id="editClientName" type="text" class="form-control" name="client_name" maxlength="40" required>
					</div>

                     <div class="form-group">
                        <label for="editlogo" class="form-label">Change Client logo</label>
                        <input class="form-control form-control-md" name="logo"  id="editlogo" type="file" accept="image/*">
                      </div>

				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
					<input type="submit" class="btn btn-info" value="Save">
				</div>
			</form>
		</div>
	</div>
</div>
