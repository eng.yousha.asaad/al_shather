<div id="addClientModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST"  action="{{route('client.store')}}"  enctype="multipart/form-data">
                @csrf
				<div class="modal-header">
					<h4 class="modal-title">Add Client</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="addClientName">Client Name</label>
						<input id="addClientName" type="text" class="form-control"  maxlength="40" name="client_name"  required>
					</div>

                     <div class="form-group">
                        <label for="addlogo" class="form-label">Client logo</label>
                        <input class="form-control form-control-md" name="logo"  id="addlogo" type="file" accept="image/*" required>
                      </div>

				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-info" data-dismiss="modal" value="Cancel">
					<input type="submit" class="btn btn-success" value="Add">
				</div>
			</form>
		</div>
	</div>
</div>
