<div id="editSubActivityModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST"  enctype="multipart/form-data">
                @csrf
                @method('put')
				<div class="modal-header">
					<h4 class="modal-title">Edit Sub Activity</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="editSubActivityName">Sub Activity Name</label>
						<input id="editSubActivityName" type="text" class="form-control" name="sub_activity_name" maxlength="40" required>
					</div>

                     <div class="form-group">
                        <label for="editimge" class="form-label">Extra Sub Activity image</label>
                        <input class="form-control form-control-md" name="images[]" multiple id="editimge" type="file" accept="image/*">
                      </div>

                      <div class="form-group">
                        <label for="editMainActivity" class="form-label">Main Activity Name</label>
                        <select class="form-select selectpicker" id="editMainActivity"  name="activity_id" required>
                            <option data-hidden="true" value="">
                                Select Main Activity
                            </option>
                            @foreach($activities as $activity)
                              <option value="{{$activity->id}}">{{$activity->activity_name}}</option>
                            @endforeach
                          </select>
                      </div>

					<div class="form-group">
						<label for="editSubActivityDescription">Sub Activity Description</label>
						<textarea id="editSubActivityDescription" class="form-control" name="description" required required></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-info" data-dismiss="modal" value="Cancel">
					<input type="submit" class="btn btn-success" value="Edit">
				</div>
			</form>
		</div>
	</div>
</div>
