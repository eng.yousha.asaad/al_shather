<div id="addSubActivityModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST"  action="{{route('sub-activity.store')}}"  enctype="multipart/form-data">
                @csrf
				<div class="modal-header">
					<h4 class="modal-title">Add Sub Activity</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="addSubActivityName">Sub Activity Name</label>
						<input id="addSubActivityName" type="text" class="form-control" name="sub_activity_name" maxlength="40" required>
					</div>

                     <div class="form-group">
                        <label for="addimge" class="form-label">Sub Activity image</label>
                        <input class="form-control form-control-md" name="images[]" multiple id="addimge" type="file" accept="image/*" required>
                      </div>

                      <div class="form-group">
                        <label for="addMainactivity" class="form-label">Main Activity Name</label>
                        <select class="form-select selectpicker"  name="activity_id"  id="addMainactivity"  required>
                            <option data-hidden="true" value="">
                                Select Main Activity
                            </option>
                            @foreach($activities as $activity)
                              <option value="{{$activity->id}}">{{$activity->activity_name}}</option>
                            @endforeach

                          </select>
                      </div>

					<div class="form-group">
						<label for="SubActivityDescription">Sub Activity Description</label>
						<textarea id="SubActivityDescription" class="form-control" name="description" required required></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-info" data-dismiss="modal" value="Cancel">
					<input type="submit" class="btn btn-success" value="Add">
				</div>
			</form>
		</div>
	</div>
</div>
