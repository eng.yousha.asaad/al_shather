<div id="addUserModal" class="modal fade addUserModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST"  action="{{route('user.store')}}"  enctype="multipart/form-data">
                @csrf
				<div class="modal-header">
					<h4 class="modal-title">Add Brand</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="addUserName">User Name</label>
						<input id="addUserName" type="text" class="form-control" name="name"  required autocomplete="name" autofocus>
					</div>


                    <div class="form-group">
						<label for="addUserEmail">User Email</label>
						<input id="addUserEmail" type="email" class="form-control" name="email"  required autocomplete="email" >
					</div>
                    <div class="form-group">
                        <label for="addUserMobile">User Mobile</label>
                        <input id="addUserMobile" type="tel" class="form-control" name="phone" value="{{ old('phone') }}" required autocomplete="phone" pattern="[0-9]+">

                    </div>


                    <div class="form-group">
						<label for="addUserCompany">User Company</label>
						<input id="addUserCompany" type="text" class="form-control" name="company"  required autocomplete="company" >
					</div>

                    <div class="form-group">
						<label for="addUserRole">User Role</label>
                        <select name="role" id="addUserRole" class="form-control form-select selectpicker" required>
                                <option value="" hidden>Select User Role</option>
                                <option value="admin">Admin</option>
                                <option value="customer">Customer</option>
                        </select>
                    </div>

                    <div class="form-group">
						<label for="addUserPassword">User Password</label>
						<input id="addUserPassword" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  required  autocomplete="new-password" >
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>

                    <div class="form-group">
						<label for="confirmPassword">Confirm Password</label>
                        <input id="confirmPassword" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
					</div>


				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-info" data-dismiss="modal" value="Cancel">
					<input type="submit" class="btn btn-success" value="Add">
				</div>
			</form>
		</div>
	</div>
</div>
