<div id="toast" class="alert alert-success" role="alert"
     style="display: none;text-align: center;position: fixed;top:0;right: 0;z-index: 1000">
    {{\Illuminate\Support\Facades\Session::get('success')}}
</div>
