<div id="editProductModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST"  enctype="multipart/form-data">
                @csrf
                @method('put')
				<div class="modal-header">
					<h4 class="modal-title">Edit Product</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="editProductName">Product Name</label>
						<input id="editProductName" type="text" class="form-control" name="product_name" maxlength="40" required>
					</div>

                     <div class="form-group">
                        <label for="editimge" class="form-label">Extra Product image</label>
                        <input class="form-control form-control-md" name="images[]" multiple id="editimge" type="file" accept="image/*">
                      </div>

                      <div class="form-group">
                        <label for="editCategory" class="form-label">Category Name</label>
                        <select class="form-select selectpicker" id="editCategory"  name="category_id" required>
                            <option data-hidden="true" value="">
                                Select Product's Category
                            </option>
                            @foreach($categories as $category)
                              <option value="{{$category->id}}">{{$category->category_name}}</option>
                            @endforeach

                          </select>
                      </div>

					<div class="form-group">
						<label for="editproductDescription">Product Description</label>
						<textarea id="editproductDescription" class="form-control" name="description"  required></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-info" data-dismiss="modal" value="Cancel">
					<input type="submit" class="btn btn-success" value="Edit">
				</div>
			</form>
		</div>
	</div>
</div>
