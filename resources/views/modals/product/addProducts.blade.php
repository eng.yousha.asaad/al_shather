<div id="addProductModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST"  action="{{route('product.store')}}"  enctype="multipart/form-data">
                @csrf
				<div class="modal-header">
					<h4 class="modal-title">Add Product</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="addProductsName">Product Name</label>
						<input id="addProductsName" type="text" class="form-control" name="product_name" maxlength="40" required>
					</div>

                     <div class="form-group">
                        <label for="addimge" class="form-label">Products image</label>
                        <input class="form-control form-control-md" name="images[]" multiple id="addimge" type="file" accept="image/*" required>
                      </div>

                      <div class="form-group">
                        <label for="addcategory" class="form-label">Category Name</label>
                        <select class="form-select selectpicker"  name="category_id"  id="addcategory"  required>
                            <option data-hidden="true" value="">
                                Select Product's Category
                            </option>

                            @foreach($categories as $category)
                              <option value="{{$category->id}}">{{$category->category_name}}</option>
                            @endforeach

                          </select>
                      </div>

					<div class="form-group">
						<label for="productDescription">Product Description</label>
						<textarea id="productDescription" class="form-control" name="description" required required></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-info" data-dismiss="modal" value="Cancel">
					<input type="submit" class="btn btn-success" value="Add">
				</div>
			</form>
		</div>
	</div>
</div>
