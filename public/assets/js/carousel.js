$(document).ready(function(){

    function imageCarousel(item , myIndex){

        let images = item.find('img');

        images.each(function(){

            if($(this).attr('data-index') == myIndex)
            {
            $(this).css('display' , 'block');
            }else
            {
                $(this).css('display' , 'none');
            }
        });

        setTimeout(imageCarousel, 2000 , item , (myIndex+1)%images.length); // Change image every 2 seconds
        }


     $('.underlay').each(function(){
        imageCarousel($(this) , 0);
     });

});
