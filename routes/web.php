<?php

use App\Http\Controllers\ActivityController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\WebsiteController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\SentenceController;
use App\Http\Controllers\SubActivityController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\UserController;
use App\Models\Brand;
use App\Models\Image;
use App\Models\Product;
use App\Models\SubActivity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
    $ar = [
        'Hello' => 'HelloX',
        'Yes' => 'YesX'
    ];
    unset($ar['Hello']);
    dd($ar);
    //send email for user contain link of new passord
    Mail::send('mail_test', array('body' => 'Test Email'),
    function ($message) {
        $message->to('eng.yousha.asaad@gmail.com');
        $message->subject('Test');
    });


     //get base sentences (in english) from languages file to transalte it
     $source = require resource_path('lang/ar/sentence.php');
     //we store translations here

     //fill temp array with all english sentences and make its translations = ' '
     foreach ($source as $key => $value) {
         $temp[$key] = '';
     }

    /*
    $prodcuts = Brand::find(1);
    dd($prodcuts->images[0]->path);
    */
    /*
    $image = new Image();
    $image->path = 'assets/images/brancd/wavetec2.png';

    $prodcuts = Brand::find(1);
    $prodcuts->images()->save($image);
    dd($prodcuts);
    */
    return view('welcome');
});

//website toutes
Route::get('/home', [WebsiteController::class , 'index']);
Route::get('/services', [WebsiteController::class , 'showServices'])->name('showService');

//panel routes
Route::resource('brand' , BrandController::class);
Route::resource('category' , CategoryController::class);
Route::resource('product' , ProductController::class);
Route::resource('image' , ImageController::class);
Route::resource('project' , ProjectController::class);
Route::resource('activity' , ActivityController::class);
Route::resource('sub-activity' , SubActivityController::class);
Route::resource('ticket' , TicketController::class);
Route::resource('file' , FileController::class);
Route::resource('user' , UserController::class);
Route::resource('client' , ClientController::class);
Route::resource('sentence' , SentenceController::class);

Route::post('/delete_brands', [BrandController::class , 'deleteGroup'])->name('delete_brands');
Route::post('/delete_clients', [ClientController::class , 'deleteGroup'])->name('delete_clients');
Route::post('/delete_categories', [CategoryController::class , 'deleteGroup'])->name('delete_categories');
Route::post('/delete_products', [ProductController::class , 'deleteGroup'])->name('delete_products');
Route::get('{brand_id}/categories' , [BrandController::class , 'showCategories'])->name('showCategories');
Route::post('/delete_activities', [ActivityController::class , 'deleteGroup'])->name('delete_activities');
Route::post('/delete_sub_activities', [SubActivityController::class , 'deleteGroup'])->name('delete_sub_activities');
Route::post('/delete_files', [FileController::class , 'deleteGroup'])->name('delete_files');
Route::get('/downloads/{type}', [FileController::class , 'show_downloads'])->name('downloads');
Route::post('/verify_all_users', [UserController::class , 'verifyGroup'])->name('verify_all_users');

Route::post('/resolve_tickets', [TicketController::class , 'resolve_all_tickets'])->name('resolve_tickets');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


//General setting

Route::get('/general', [App\Http\Controllers\GeneralSetting::class, 'index'])->name('general_setting');
Route::get('/updated_ticekts_time', [App\Http\Controllers\GeneralSetting::class, 'update_times'])->name('update_times');
Route::get('/change_language', [App\Http\Controllers\GeneralSetting::class, 'change_language'])->name('change_language');

